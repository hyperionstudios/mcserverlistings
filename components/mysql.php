<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name mysql.php
*/
class mysql {
    protected $config = null;
    protected $mysqli = null;

	public function __construct($config) {
        $this->config = $config;
	}
    public function connect() {
        if ($this->mysqli == null) {
            $this->mysqli = new mysqli($this->config->mysql_host,$this->config->mysql_user,
            $this->config->mysql_pass,$this->config->mysql_database);
            if(mysqli_connect_errno()){
                print "<h3 align='center'>" . mysqli_connect_error() . "</h3>";
            }
        }
    }
	public function query($string) {
        $this->connect();
        $result = $this->mysqli->query($string) or print "ERROR: ". $this->mysqli->error ."<br/>";
		return $result;
	}

    public function escape($t) {
        $this->connect();
        return $this->mysqli->real_escape_string($t);
    }
    public function getLink() { 
        $this->connect();
        return $this->mysqli; 
    }
	function __destruct() {
	   if ($this->mysqli != null)
            $this->mysqli->close();
	}
}
?>