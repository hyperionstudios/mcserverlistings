<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name server.php
 */

 require_once("components/util/util.php");
 require_once("components/var/votifier.php");
 
 class Server {
    private static $myStInsert;
    
    protected $id;
    protected $isOnline = false;
    protected $ping;
    protected $name;
    protected $ownerId;
    protected $logo;
    protected $motd;
    protected $playerslots;
    protected $unsafeDescription;
    protected $safedDescription;
    protected $ip;
    protected $port;
    protected $cookies;
    
    protected $isSponsered = false;
    protected $sponserStartTime;
    protected $sponserEndTime;
    
    protected $tags;
    protected $website;
    protected $continent;
    protected $timeAdded;

    protected $allowComments;

    protected $downtime;
    protected $uptime;
    protected $formatedUptime;
    protected $lastcheck;

    protected $hasVotifier;
    protected $votifierIp;
    protected $votifierPort;
    protected $votifierRSAKey;
     
    public function __construct($id, $ownerId, $servername, $logo, $motd, $unsafeDescription, $safeDescription,
        $ip, $port, $website, $continent, $tags, 
        $cookies=0, $lastcheck=0, $allowComments=0, $time=0,
        $sponserStartTime=0, $sponserEndTime=0, $ping=0, $downtime=0, $uptime=0, $formateduptime="",
        $playerslots="", $votifier=0, $votifierIp="", $votifierPort=0, $votifierRSAKey="") {
        global $mysql;
        $this->id = intval($id);
        $this->ownerId = intval($ownerId);
        $this->name = $servername;
        $this->logo = $logo;
        $this->motd = $motd;
        $this->playerslots = $playerslots;
        
        $this->unsafeDescription = $unsafeDescription;
        $this->safeDescription = $safeDescription;
        $this->ip = $ip;
        $this->port = intval($port);
        $this->cookies = intval($cookies);
        $this->website = $website;
        $this->continent = $continent;
        
        $this->sponserStartTime = intval($sponserStartTime);
        $this->sponserEndTime = intval($sponserEndTime);
        
        $this->ping = intval($ping);
        $this->downtime = intval($downtime);
        $this->uptime = intval($uptime);
        $this->formatedUptime = $formateduptime;
        $this->lastcheck = intval($lastcheck);
        $this->isOnline = intval($ping != 0);
        if (is_array($tags)) {
            $this->tags = $tags;
        }
        else {
            $this->tags = explode(",",$tags);
        }
        $this->hasVotifier = intval($votifier);
        $this->votifierIp = $votifierIp;
        $this->votifierPort = intval($votifierPort);
        $this->votifierRSAKey = $votifierRSAKey;
        
        $this->allowComments = intval($allowComments);
        $this->timeAdded = intval($time);
    }

    public function setId($id) { $this->id = intval($id); }

    public function getId() { return $this->id; }
    public function getName() { return $this->name; }
    public function getLogo() { return $this->logo; }
    public function getMotd() { return $this->motd; }
    public function getPlayerSlots() { return $this->playerslots; }
    public function getOwnerId() { return $this->ownerId; }
    
    public function getUnsafeDescription() { return $this->unsafeDescription; }
    public function getSafeDescription() { return $this->safeDescription; }
    public function getWebsite() { return $this->website; }
    public function getContinent() { return $this->continent; }
    public function getPort() { return $this->port; }
    public function getIp() { return $this->ip; }
    public function getCookies() { return $this->cookies; }
    public function getTags() { return $this->tags; }
    public function getTimeAdded() { return $this->timeAdded; }
    public function getPing() { return $this->ping; }
    public function getUptime() { return $this->uptime; }
    public function getDowntime() { return $this->downtime; }
    public function getLastcheck() { return $this->lastcheck; }
    public function isOnline() { return $this->isOnline; }
    public function isSponsered() { return !$this->isSponserShipExpired(); }
    public function isSponserShipExpired() {
         return $this->sponserEndTime <= time();
    }
    public function isNew() {
        //one week.
        return ($this->timeAdded + 604800) >= time();
    }
    public function allowComments() { return $this->allowComments; }

    public function hasVotifier() { return $this->hasVotifier; }
    public function getVotifierIp() { return $this->votifierIp; }
    public function getVotifierPort() { return $this->votifierPort; }
    public function getVotifierRSAKey() { return $this->votifierRSAKey; }
    
    public function hasTag($tag) {
        $count = count($this->tags);
        for ($i = 0; $i < $count; $i++) {
            if ($this->tags[$i] == $tag) {
                return true;
            }
        }
        return false;
    }
    public function getFormatedUptime() { return $this->formatedUptime; }
    
    public function getTagsString() { 
        $t = "";
        $count = count($this->tags);
        for ($i = 0; $i < $count; $i++) {
            if (!empty($this->tags[$i])) {
                if ($i != 0) { $t .= ","; }
                $t .= $this->tags[$i];
            }
        }
        return $t; 
    }
    
    public static function calcUptime($up,$down) {
        /**
        D = 2 X 60 minutes = 120 minutes
         T = 30 days X 24 hours X 60 minutes = 43200 minutes

         Downtime (in %)
         = [120/43200] X 100
         = 0.2777 %

         Uptime (in %)
         = 100 - 0.2777
         = 99.7222 %
        */
        $u = 0;
        if ($up) {
            if ($down) {
                $d = ($down / $up) * 100;
                $u = round(100 - $d,2);
                if ($u < 0) { $u = 0; }
                if ($u > 100) { $u=100; }
            }
            else {
                $u = 100;
            }
        }
        return $u."%";
    }

    public function giveCookie() {
        global $mysql;
        ++$this->cookies;
        $mysql->query("UPDATE Servers SET cookies='$this->cookies' WHERE id='$this->id'");
    }
    /* Sponser functions */
    public function giveSponsership() {
        global $config, $mysql;
        if ($this->isSponsered) {
            $this->sponserEndTime += $config->sponsershipTime;
        }
        else {
            $this->sponserEndTime = time() + $config->sponsershipTime;
            $this->sponserStartTime = time();
        }
        $this->isSponsered = true;
        $mysql->query("UPDATE Servers SET sponserStartTime='$this->sponserStartTime',sponserEndTime='$this->sponserEndTime' WHERE id='$this->id'");
    }
    public function removeSponsership() {
        $this->isSponsered = false;
        $mysql->query("UPDATE Servers SET sponserStartTime='0',sponserEndTime='0' WHERE id='$this->id'");
    }
    

    /* Static Methods */
    
    public static function getNextServerId() {
        global $mysql;
        /* Returns the next id when someone submits one! */
        $result = $mysql->query("SELECT MAX(id) FROM Servers");
        $row = $result->fetch_row();
        return $row[0];
    }
    
    public static function serversFromResult($result) {
        $servers = array();
        while ($rows = mysqli_fetch_array($result)) {
            $server = new Server(
                $rows['id'],
                $rows['owner'],
                $rows['name'],
                $rows['logo'],
                $rows['motd'],
                $rows['unsafedescription'],
                $rows['safedescription'],
                $rows['ip'],
                $rows['port'],
                $rows['website'],
                $rows['continent'],
                $rows['tags'],
                $rows['cookies'],
                $rows['lastcheck'],
                $rows['comments'],
                $rows['time'],
                $rows['sponserStartTime'],
                $rows['sponserEndTime'],
                $rows['ping'],
                $rows['downtime'],
                $rows['uptime'],
                $rows['formateduptime'],
                $rows['playerslots'],
                $rows['votifier'],
                $rows['votifierIp'],
                $rows['votifierPort'],
                $rows['votifierRSAKey']
            );
            array_push($servers,$server);
        }
        return $servers;
    }
    public static function getServerCount() {
        global $mysql;
        $result = $mysql->query("SELECT COUNT(*) FROM Servers");
        $row = mysqli_fetch_array($result);
        return $row['COUNT(*)'];
    }
    public static function getSponseredServerCount() {
        global $mysql;
        $result = $mysql->query("SELECT DISTINCT COUNT(*) FROM Servers WHERE sponserEndTime >= '".time()."'");
        $row = mysqli_fetch_array($result);
        return $row['COUNT(*)'];
    }

    public static function getServers() {
        global $mysql;
        $result = $mysql->query("SELECT * FROM Servers ORDER BY cookies DESC");
        return Server::serversFromResult($result);
    }

    public static function getLimitServers($start,$end) {
        global $mysql;
        $servers = array();
        $result = $mysql->query("SELECT * FROM Servers ORDER BY cookies DESC LIMIT ". $start .", ". $end);
        return Server::serversFromResult($result);
    }

    public static function getRandServer() {
        global $mysql;
        $result = $mysql->query("SELECT * FROM Servers ORDER BY Rand() LIMIT 1");
        $servers = Server::serversFromResult($result);
        return $servers[0];
    }

    public static function getRandSponseredServer($sp) {
        global $mysql;
        $result = $mysql->query("SELECT * FROM Servers WHERE sponserEndTime >= '".time()."' ORDER BY Rand() LIMIT 1");
        $servers = Server::serversFromResult($result);
        return $servers[0];
    }
    /**
     * simple but slow fetch random server.
     */
    public static function slowRandSponseredServers($count) {
        global $mysql;
        $servers = array();
        $result = $mysql->query("SELECT * FROM Servers WHERE sponserEndTime >= '".time()."' ORDER BY Rand() DESC, cookies DESC LIMIT $count");
        return Server::serversFromResult($result);
    }
    /**
    * a complicated thing that should get a random servers in order. :S
    */
    public static function getNonSqlRandSponseredServers() {
        global $mysql,$config;
        $ids = array();
        /** Shouldn't be to many...right? */
        $sql = "SELECT * FROM Servers WHERE sponserEndTime >= '".time()."' LIMIT 0, {$config->maxSponsers}";
        $result = $mysql->query($sql);
        $s = util::shuffler(Server::serversFromResult($result));
        
        $servers = array();
        $count = count($s);
        for ($i = 0; $i < $count; $i++) {
            if ($i >= $config->maxDisplaySponseredServers) { break; }
            array_push($servers, $s[$i]);
        }
        usort($servers,array("Server", "sortByCookies"));
        return $servers;
    }
    /**
     * an USORT function to sort server's by cookies
     */
    public static function sortByCookies($serverA, $serverB) {
        if (($serverA->getCookies() == $serverB->getCookies())) { return 0; }
        return ($serverA->getCookies() > $serverB->getCookies()) ? -1 : 1;
    }

    public static function getSponseredServers() {
        global $mysql;
        $servers = array();
        $result = $mysql->query("SELECT * FROM Servers WHERE sponserEndTime >= '".time()."' ORDER BY cookies DESC");
        return Server::serversFromResult($result);
    }

    public static function getSponseredServerByID($id) {
        global $mysql;
        $result = $mysql->query("SELECT * FROM Servers WHERE id='$id' LIMIT 1");
        if ($result->num_rows) {
            $servers = Server::serversFromResult($result);
            return $servers[0];
        }
        return null;
    }

    public static function getServerByID($id) {
        global $mysql;
        $result = $mysql->query("SELECT * FROM Servers WHERE id='$id' LIMIT 1");
        if ($result->num_rows) {
            $servers = Server::serversFromResult($result);
            return $servers[0];
        }
        return null;
    }
    public static function delete($server) {
        global $mysql,$config;
        $sql = "DELETE FROM Servers WHERE id='".$server->getId()."'";
        $mysql->query($sql);
        require_once("components/util/logostatus.php");
        LogoStatus::delete($server);
        require_once("components/var/comment.php");
        Comment::deleteFromServer($server->getId());
    }
    public static function insertIntoDB($server) {
        global $mysql,$purifier;
        if (self::$myStInsert == null) {
             $sql = "INSERT INTO Servers (name,owner,logo,motd,playerslots,ping,lastcheck,uptime,
             ip,port,website,continent,unsafedescription,safedescription,comments,cookies,time,tags,
             votifier,votifierIp,votifierPort,votifierRSAKey)
             VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
             //print $sql;
            self::$myStInsert = $mysql->getLink()->prepare($sql);
            self::$myStInsert->bind_Param('sisssiiisisssiiisisis',$name,$ownerid,$logo,$motd,$playerslots,
            $ping,$lastcheck,$uptime,$ip,$port,$website,$continent,$unsafeDescription,$safeDescription,$allowComments,
            $cookies,$time,$tags,$allowVotifer,$votifierIp,$votiferPort,$votifierRSAKey);
        }
        $name = htmlentities($server->getName());
        $ownerid = $server->getOwnerId();
        $logo = htmlentities($server->getLogo());
        $motd = $server->getMotd();
        $playerslots = $server->getPlayerSlots();
        $ping = $server->getPing();
        $lastcheck = $server->getLastcheck();
        $uptime = $server->getUptime();
        $ip = $server->getIp();
        $port = $server->getPort();
        $website = htmlentities($server->getWebsite());
        $continent = htmlentities($server->getContinent());
        $unsafeDescription = $server->getUnsafeDescription();
        //make sure we exscape this ;3
        $safeDescription = $mysql->escape($purifier->purify($server->getSafeDescription()));
        $allowComments = $server->allowComments();
        $cookies = $server->getCookies();
        $time = $server->getTimeAdded();
        $tags = $server->getTagsString();
        $allowVotifer = $server->hasVotifier();
        $votifierIp = htmlentities($server->getVotifierIp());
        $votiferPort = $server->getVotifierPort();
        $votifierRSAKey = $server->getVotifierRSAKey();
        
        self::$myStInsert->execute();
        
        $server->setId($mysql->getLink()->insert_id);
    }
    public static function create($name,$user,$ip,$port,$website,$continent,$unsafeDescription,$safeDescription,$allowComments,$logo,$tags,
        $votifier,$votifierIp,$votifierPort,$votifierRSAKey) {
        global $mysql,$config;
        if ($user != null) {
            $motd = "";
            $playerslots = "N/A";
            $p = 0;
            $up = 0;
            $ping = Util::pingWithExtra($ip,$port,300);
            if ($ping != false) {
                $motd = $ping['motd'];
                $playerslots = $ping['players'] ."/".$ping['max_players'];
                $p = $ping['time'];
                $up = 1;
            }
            $server = new Server(0,$user->getId(),$name,$logo,$motd,$unsafeDescription,$safeDescription,$ip,$port,
            $website,$continent,$tags,0,time(),$allowComments,time(),0,0,$ping,0,0,
            ($up == 1) ? "100%" : "0%",$playerslots,$votifier,$votifierIp,$votifierPort,$votifierRSAKey);
            
            self::insertIntoDB($server);
            return $server;
        }
        return null;
    }
}