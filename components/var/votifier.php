<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name votifier.php
 */
 require_once("components/var/server.php");
class Votifier {
    public static function sendServerInfo($server,$userIGN,$userIp) {
        global $config;
        if ($server->hasVotifier()) {
            // Details of the vote.
            $str = "VOTE\n" .
                   $config->site_name."\n" .
                   $userIGN."\n" .
                   $userIp."\n" .
                   time()."\n";
            // Fill in empty space to make the encrypted block 256 bytes.
            $leftover = (256 - strlen($str)) / 2;

            while ($leftover > 0) {
                $str .= "\x0";
                $leftover--;
            }
            // The public key, this is an example.
            $key = $server->getVotifierRSAKey();
            $key = wordwrap($key, 65, "\n", true);
            $key = "-----BEGIN PUBLIC KEY-----\n".$key."\n-----END PUBLIC KEY-----";
            // Encrypt the string.
            openssl_public_encrypt($str, $encrypted, $key);
            // Establish a connection to Votifier.
            $socket = fsockopen($server->getVotifierIp(), $server->getVotifierPort(), $errno, $errstr, 2);
            if (!$socket) {
                return false;
            }
            // Send the contents of the encrypted block to Votifier.
            fwrite($socket, $encrypted);
            return true;
        }
        return false;
    }
}
 ?>