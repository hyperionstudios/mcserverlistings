<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name comment.php
 */
 require_once("components/var/server.php");
 require_once("components/var/user.php");
 class Comment {
    protected $id;
    protected $serverId;
    protected $ownerId;
    protected $msg;
    protected $time;

    public function __construct($id, $serverId,$ownerId,$msg,$time) {
            $this->id = $id;
            $this->serverId = $serverId;
            $this->ownerId = $ownerId;
            $this->msg = $msg;
            $this->time = $time;
    }
    public function getId() { return stripslashes($this->id); }
    public function getOwnerId() { return $this->ownerId; }
    public function getServerId() { return stripslashes($this->serverId); }
    public function getMSG() { return $this->msg; }
    public function getTime() { return $this->time; }

    public static function CommentsFromResult($result) {
        $comments = array();
        while ($rows = mysqli_fetch_array($result)) {
            array_push($comments,
            new Comment($rows['id'], $rows['serverId'], $rows['ownerId'], $rows['msg'],$rows['time'])
            );
        }
        return $comments;
    }
    public static function getCommentsByUser($user) {
        $result = $mysql->query("SELECT * FROM Comments WHERE ownerId='". $user->getId()."'");
        return Comments::CommentsFromResult($result);
    }
    public static function getCommentsByServer($server) {
        $result = $mysql->query("SELECT * FROM Comments WHERE serverId='". $server->getId()."'");
        return Comments::CommentsFromResult($result);
    }
    public static function getCommentFromId($id) {
        global $mysql,$config;
        $id = $mysql->escape($id);
        $result = $mysql->query("SELECT * FROM Comments WHERE id='$id'");
        if ($result->num_rows) {
            $comment = Comment::CommentsFromResult($result);
            return $comment[0];
        }
        return null;
    }
    public static function deleteFromServer($serverid) {
        global $mysql,$config;
        $sql = "DELETE FROM Comments WHERE serverId='$serverid'";
        $mysql->query($sql);
    }
    public static function delete($id) {
        global $mysql,$config;
        $sql = "DELETE FROM Comments WHERE id='$id'";
        $mysql->query($sql);
    }
    public static function create($user,$server,$msg) {
        global $mysql,$config;
        $time = time();
        $sql = "INSERT INTO Comments (ownerId,serverId,msg,time) VALUES ('".$user->getId() ."',
            '". $server->getId() ."','$msg','$time')";
        $mysql->query($sql);
    }

}