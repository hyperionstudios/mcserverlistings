<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name guestuser.php
 */
 require_once("components/util/encyrptor.php");

 class GuestUser {
    protected $id;
    protected $ip;
    protected $ign;
    protected $server_votes;

    public function __construct($id,$ign, $ip,$serverVotes) {
            global $mysql,$config;
            $this->id = $id;
            $this->ip = $ip;
            $this->ign = $ign;
            $serverVotes = explode(",",$serverVotes);
            $count = count($serverVotes);
            for($i = 0; $i < $count; $i++) {
                $t = explode(":",$serverVotes[$i]);
                if (count($t) >= 2) {
                    $this->server_votes[$t[1]] = $serverVotes[$i];
                }
            }
    }

    /*
    *Limits the user votage
    * returns 0 if succeded
    * else returns the time left to wait. unformated
    */
    public function vote($server) {
        if ($server != null) {
            $time = $this->hasVotedFor($server);
            if ($time == 0) {
                $this->server_votes[$server->getId()] = time() .":". $server->getId();
                $server->giveCookie();
                if ($server->hasVotifier()) {
                    Votifier::sendServerInfo($server,empty($this->ign) ? "HeroBrine" : $this->getIGN(),$this->getIp());
                }
                $this->UpdateServerVote();
                return $time;
            }
            return $time;
        }
    }

    public function getId() { return $this->id; }
    public function getIGN() { return stripslashes($this->ign); }
    public function getIp() { return stripslashes($this->ip); }

    public function UpdateServerVote() {
        global $mysql,$config;
        $s = "";
        foreach($this->server_votes as $serverV) {
            $t = explode(":",$serverV);
            if (count($t) >= 2) {
                $time = $t[0] + $config->hoursUntilCanVoteAgain;
                if ($time >= time()) { // hasn't expired so add it.
                    $s .= $serverV;
                    $s .= ",";
                }
            }
        }
        $mysql->query("UPDATE GuestUsers SET servervotes='$s' WHERE id='$this->id'");
    }

    public function hasVotedFor($server) {
        global $mysql,$config;
        if (!empty($this->server_votes[$server->getId()])) {
            $s = $this->server_votes[$server->getId()];
            $t = explode(":",$s);
            //$t[0] = time $t[1] = serverid
            if (count($t) >= 2) {
                $time = $t[0] + $config->hoursUntilCanVoteAgain;
                if ($time >= time()) {
                    return $time;
                }
            }
        }
        return 0;
    }

    public static function UsersFromResult($result) {
        $users = array();
        while ($rows = mysqli_fetch_array($result)) {
            array_push($users,
            new GuestUser($rows['id'],$rows['IGN'],$rows['ip'],$rows['servervotes'])
            );
        }
        return $users;
    }
    public static function getUserFromIGN($ign) {
        global $mysql,$config;
        $ign = $mysql->escape($ign);
        $result = $mysql->query("SELECT * FROM GuestUsers WHERE IGN='$ign'");
        if ($result->num_rows) {
            $users = GuestUser::UsersFromResult($result);
            return $users[0];
        }
        return null;
    }
    public static function getUserFromIp($ip) {
        global $mysql,$config;
        $result = $mysql->query("SELECT * FROM GuestUsers WHERE ip='$ip'");
        if ($result->num_rows) {
            $users = GuestUser::UsersFromResult($result);
            return $users[0];
        }
        return null;
    }
    public static function getUserFromIpOrIGN($ip,$ign) {
        global $mysql,$config;
        $sql = "SELECT * FROM GuestUsers WHERE ip='$ip'";
        if (!empty($ign)) { $sql .= " OR ign='$ign'"; }
        $result = $mysql->query($sql);
        if ($result->num_rows) {
            $users = GuestUser::UsersFromResult($result);
            return $users[0];
        }
        return null;
    }
    public static function getUserFromId($id) {
        global $mysql,$config;
        $id = $mysql->escape($id);
        $result = $mysql->query("SELECT * FROM GuestUsers WHERE id='$id'");
        if ($result->num_rows) {
            $users = GuestUser::UsersFromResult($result);
            return $users[0];
        }
        return null;
    }
    public static function createGuestUser($ign,$ip) {
        global $mysql,$config;
        $sql = "INSERT INTO GuestUsers (IGN,ip)
         VALUES ('$ign','$ip')";
        $mysql->query($sql);
        return new GuestUser($mysql->getLink()->insert_id,$ign,$ip,null);
    }


}