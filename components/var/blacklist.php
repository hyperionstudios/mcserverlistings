<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name blacklist.php
 */
 class BlacklistUser {
    protected $id;
    protected $ip;
    protected $reason;

    public function __construct($id, $ip,$reason) {
        global $mysql,$config;
        $this->id = $id;
        $this->ip = $ip;
        $this->reason = $reason;
    }

    public function getId() { return $this->id; }
    public function getIp() { return $this->ip; }
    public function getReason() { return $this->reason; }

    public static function BlacklistsFromResult($result) {
        $users = array();
        while ($rows = mysqli_fetch_array($result)) {
            array_push($users,
            new BlacklistUser($rows['id'],$rows['ip'],$rows['reason'])
            );
        }
        return $users;
    }
    public static function getBlacklistFromIp($ip) {
        global $mysql,$config;
        $result = $mysql->query("SELECT * FROM Blacklist WHERE ip='$ip'");
        if ($result->num_rows) {
            $users = BlacklistUser::BlacklistsFromResult($result);
            return $users[0];
        }
        return null;
    }
    public static function createUser($ign,$ip) {
        global $mysql,$config;
        $sql = "INSERT INTO Blacklist (ip)
         VALUES ('$ip')";
        $mysql->query($sql);
    }
    public static function isBlacklisted($ip) {
        return BlacklistUser::getBlacklistFromIp($ip) != null;
    }
}