<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name user.php
 */
 require_once("components/util/encyrptor.php");
 require_once("components/var/guestuser.php");
 /**
  * we extend GuestUser cause it uses methods
  *  that was also common with this class.
  */
 class User extends GuestUser {
    protected $isLoggedIn = false;
    protected $name;
    protected $email;
    protected $pass;
    protected $verifyKey;
    protected $isVerified = false;
    protected $isAdmin = false;
    protected $sponserTokens;

    public function __construct($id, $name,$ign,$email,$pass,$ip,$verifyKey,$isVerified,$isAdmin, $serverVotes,$sponserTokens) {
            global $mysql,$config;
            parent::__construct($id,$ign,$ip,$serverVotes);
            $this->name = $name;
            $this->email = $email;
            $this->pass = $pass;
            $this->verifyKey = $verifyKey;
            $this->isVerified = $isVerified;
            $this->isAdmin = $isAdmin;
            $this->sponserTokens = $sponserTokens;
    }

    public function login($pass) {
        global $config;
        $encyptor = new encryptor($config);
        $pass = $encyptor->SHA256Hash($pass);
        if ($this->pass == $pass) {
            $this->isLoggedIn = true;
         }
         else {
            $this->isLoggedIn = false;
         }
         return $this->isLoggedIn;
    }

    public function getName() { return stripslashes($this->name); }
    public function getPass() { return $this->pass; }
    public function getEmail() { return stripslashes($this->email); }
    public function getVerifyKey() { return $this->verifyKey; }
    public function getTokens() { return $this->sponserTokens; }
    public function isLoggedIn() { return $this->isLoggedIn; }
    public function isAdmin() { return $this->isAdmin; }
    public function isVerified() { return $this->isVerified; }
    public function Verify() { $this->isVerified = true; }
    
    public function UpdateServerVote() {
        global $mysql,$config;
        $s = "";
        foreach($this->server_votes as $serverV) {
            $t = explode(":",$serverV);
            if (count($t) >= 2) {
                $time = $t[0] + $config->hoursUntilCanVoteAgain;
                if ($time >= time()) { // hasn't expired so add it.
                    $s .= $serverV;
                    $s .= ",";
                }
            }
        }
        $mysql->query("UPDATE Users SET servervotes='$s'WHERE id='$this->id'");
    }
    public function giveToken($tokens = 1) {
        global $mysql,$config;
        $this->sponserTokens += $tokens;
        $mysql->query("UPDATE Users SET sponserTokens='$this->sponserTokens' WHERE id='$this->id'");
    }
    public function useToken($tokens = 1) {
        global $mysql,$config;
        $t = $this->sponserTokens - $tokens;
        if ($t >= 0) {
            $this->sponserTokens = $t;
            $mysql->query("UPDATE Users SET sponserTokens='$this->sponserTokens' WHERE id='$this->id'");
            return true;
        }
        return false;
    }
    

    public static function UsersFromResult($result) {
        $users = array();
        while ($rows = mysqli_fetch_array($result)) {
            array_push($users,
            new User($rows['id'], $rows['name'],$rows['IGN'],$rows['email'],$rows['password'],
                $rows['ip'],$rows['verifykey'],$rows['verified'],$rows['admin'],$rows['servervotes'],
                $rows['sponserTokens'])
            );
        }
        return $users;
    }
    public static function getUser($name) {
        global $mysql,$config;
        $name = $mysql->escape($name);
        $result = $mysql->query("SELECT * FROM Users WHERE name='$name' LIMIT 1");
        if ($result->num_rows) {
            $users = User::UsersFromResult($result);
            return $users[0];
        }
        return null;
    }
    public static function getUserFromId($id) {
        global $mysql,$config;
        $id = $mysql->escape($id);
        $result = $mysql->query("SELECT * FROM Users WHERE id='$id' LIMIT 1");
        if ($result->num_rows) {
            $users = User::UsersFromResult($result);
            return $users[0];
        }
        return null;
    }
    public static function delete($id) {
        global $mysql,$config;
        $sql = "DELETE FROM Users WHERE id='$id'";
        $mysql->query($sql);
    }
    public static function createUser($username, $ign,$password,$email, $ip) {
        global $mysql,$config;
        $encyptor = new encryptor($config);
        $pass = $encyptor->SHA256Hash($password);
        $verifykey = $encyptor->SHA256Hash(uniqid($username,true));
        $sql = "INSERT INTO Users (name,IGN,password,email,ip,verifykey)
         VALUES ('$username','$ign','$pass','$email','$ip','$verifykey')";
        $mysql->query($sql);
    }
    public static function emailVerification($username,$ign,$email,$key) {
        global $config;
        require_once("components/util/smtp.php");
        // subject
        $subject = "Verification to $config->site_name";
        // message
        $url = $config->site_url ."/verify.php?key=$key";
        $message = "<html>
            <head>
                <title>Verification to $config->site_name</title>
            </head>
            <body>
                Welcome $username to $config->site_name.<br/>
                Username: $username <br/>
                IGN: $ign <br/>
                click the link to verify your email.<br/>
                if the link doesn't work. paste it into your address bar.<br/>
                <a href='$url'>$url</a><br/>
                Have any problems, please email us at <a href='mailto:$config->site_email'>$config->site_email</a>.</br>
                <br/>
                <span style='font:8px;'>If you didn't register for this site,
                please reply this and explain that you did not register for instant removal.</span>
            </body>
        </html>";

        $smtp = new SMTPClient($config->SmtpServer,$config->SmtpPort,$config->SmtpUser,$config->SmtpPass,
        $config->site_email,$email,$subject,$message);
        $smtp->SendMail();
    }

}