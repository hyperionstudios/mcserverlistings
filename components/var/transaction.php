<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name transactions.php
 */
 class Transaction {
    protected $id;
    protected $transactionID;
    protected $orderTime;
    protected $amount;
    protected $ownerId;

    public function __construct($id, $transactionID,$orderTime,$amount,$ownerId) {
        global $mysql,$config;
        $this->id = $id;
        $this->transactionID = $transactionID;
        $this->amount = $amount;
        $this->orderTime = $orderTime;
        $this->ownerId = $ownerId;
    }

    public function getId() { return $this->id; }
    public function getTransactionsID() { return $this->transactionID; }
    public function getAmount() { return $this->amount; }
    public function getVerifyKey() { return $this->verifyKey; }
    public function getOrderTime() { return $this->orderTime; }
    public function getOwnerId() { return $this->ownerId; }
    
    public static function TransactionsFromResult($result) {
        $transactions = array();
        while ($rows = mysqli_fetch_array($result)) {
            array_push($transactions,
            new Transaction($rows['id'], $rows['transactionId'],$rows['amount'],$rows['orderTime'],$rows['ownerId'])
            );
        }
        return $transactions;
    }
    public static function getTransactionByTransId($transactionId) {
        global $mysql,$config;
        $result = $mysql->query("SELECT * FROM Transactions WHERE transactionId='$transactionId' LIMIT 1");
        if ($result->num_rows) {
            $transactions = Transaction::TransactionsFromResult($result);
            return $transactions[0];
        }
        return null;
    }
    public static function getTransactionsFromUserId($id) {
        global $mysql,$config;
        $result = $mysql->query("SELECT * FROM Transactions WHERE ownerId='$id'");
        return Transaction::TransactionsFromResult($result);
    }
    public static function delete($id) {
        global $mysql,$config;
        $sql = "DELETE FROM Transactions WHERE id='$id'";
        $mysql->query($sql);
    }
    public static function createTransaction($transactionId, $orderTime,$amount,$ownerId) {
        global $mysql,$config;
        $sql = "INSERT INTO Transactions (transactionId,orderTime,amount,ownerId)
         VALUES ('$transactionId', '$orderTime','$amount','$ownerId')";
        $mysql->query($sql);
    }

}