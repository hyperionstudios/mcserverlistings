<?php

class HTMLPurifier_Filter_YouTube extends HTMLPurifier_Filter
{

    public $name = 'YouTube';
//<iframe src="http://www.youtube.com/embed/fMyL8FAJ9PY" frameborder="0" width="1280" height="720"></iframe>
    public function preFilter($html, $config, $context) {
        $pre_regex = '#(http://www.youtube.com/)(v/|embed/|watch\?v\=)([-|~_0-9A-Za-z]+)(.*>)#i';
        //preg_match($pre_regex,$html,$output);
        //var_dump($output);

        $pre_replace = '<span class="youtube-embed">\3</span>';
        //echo preg_replace($pre_regex, $pre_replace, $html);
        return preg_replace($pre_regex, $pre_replace, $html);
    }

    public function postFilter($html, $config, $context) {
        $post_regex = '#<span class="youtube-embed">([-|~_0-9A-Za-z]+)</span>#i';
        return preg_replace_callback($post_regex, array($this, 'postFilterCallback'), $html);
    }

    protected function armorUrl($url) {
        return str_replace('--', '-&#45;', $url);
    }

    protected function postFilterCallback($matches) {
        $url = $this->armorUrl($matches[1]);
        //print_r($matches);
        return "<iframe width='640' height='360' src='http://www.youtube.com/embed/{$url}' frameborder='0' allowfullscreen></iframe>";

    }
}

// vim: et sw=4 sts=4
