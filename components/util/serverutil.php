<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name serverutil.php
 */
 class serverUtil {
    static function simpleDisplayServer($server, $displayNew = true) {
        if ($server->isSponsered()) { print "<div class='bubble ServerListItem SponseredServerListItem'>"; }
        else { print "<div class='bubble ServerListItem'>"; }
        ?>
        <table class='ServerItemTable'>
            <tbody>
                <tr>
                    <td class='ServerTitle
                    <?php
                    if ($server->isSponsered()) { print " SponseredServerTitle'>"; }
                    else { print "'>"; }

                    if ($server->isNew() && $displayNew) {
                        print "<span class='ServerNew'>
                            <img src='/template/Styles/Images/new.png' title='New!' width='50'/>
                        </span>\n";
                    }
                    ?>
                    <a class='bubble' href='serverprofile.php?id=<?php print $server->getId(); ?>'>
                        <?php print htmlentities($server->getName()); ?>
                        <span style='float:left;'>
                        <?php
                         if ($server->isOnline()) {
                            print "<img src='template/Styles/Images/online.png' title='Online!' alt='Online!' />";
                         }
                         else {
                            print "<img src='template/Styles/Images/offline.png' title='Offline :(' alt='Offline!' />";
                         }
                        ?>
                        </span>
                    </a>
                    </td>
                    <td width='40' align='right'>
                        <em><?php print $server->getCookies(); ?></em>
                        <img style='float:right;' src='template/Styles/Images/Cookie.png' title='COOKIE?!!?1!21' alt='Cookie'/>
                    </td>
                    <td class='ServerIp' onclick='javascript:prompt("Copy this and enter it in your minecraft.","<?php print htmlentities($server->getIp() .":". $server->getPort()); ?>");' title='Click Here to copy ip.'>
                        <strong> Minecraft Server IP:</strong>
                        <em>
                            <?php print htmlentities($server->getIp()); ?>
                        </em>
                        <strong>Port:</strong> <em><?php print htmlentities($server->getPort()); ?></em>
                    </td>
                    <td class='ServerContinent'>
                        <strong>Continent:</strong> <br /><em><?php print $server->getContinent(); ?></em>
                    </td>
                    <td class='ServerPlayerSlots'>
                        <strong>Player Slots:</strong> <br /><em><?php print $server->getPlayerSlots(); ?></em>
                    </td>
                </tr>
                <tr>
                    <td class='ServerTags' colspan='5'>
                        <strong>Tags:</strong>
                        <?php
                        $tags = $server->getTags();
                        $count = count($tags);
                        for ($i = 0; $i < $count; $i++) {
                            print "[". $tags[$i] ."] ";
                        }
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php
    }
}