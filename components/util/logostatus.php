<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name logostatus.php - creates small images to display server status
*/
class LogoStatus {
    public static $path = "logostatus/status_%s.png";
    public static function getImage($server) {
        if ($server != null) {
            return sprintf(LogoStatus::$path,$server->getId());
        }
        return false;
    }
    public static function delete($server) {
        unlink(LogoStatus::getImage($server));
    }
    public static function create($server) {
        if ($server != null) {
            $size = getimagesize("logostatus/bg.png");
            $img = imagecreatefrompng("logostatus/bg.png");
            $len = strlen($server->getName());
            imagestring($img, 30, ($size[0] / 2) - (($len / 2)*3), 4, $server->getName() , 255);
            imagestring($img, 2, 64, 18,$server->getIp() .":". $server->getPort(),255);
            imagestring($img, 2, 64, 32,$server->getPlayerSlots(),255);
            imagestring($img, 2, 64, 48,$server->getFormatedUptime(),255);
            $color = 0;
            $status = "Online";
            if ($server->isOnline()) {
                $color = imagecolorallocate($img,0,140,0);
            }
            else {
                $color = imagecolorallocate($img,217,0,0);
                $status = "Offline";
            }
            imagestring($img, 12, 64, 64, $status, $color);
            imagepng($img,sprintf(LogoStatus::$path,$server->getId()),6,0);
            imagedestroy($img);
        }
    }
}
 ?>