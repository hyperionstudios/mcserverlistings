<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name encryptor.php - simple hashing class.
*/
class encryptor {
    protected $config;

   	public function __construct($config) {
		$this->config = $config;
	}
    public function SHA256Hash($input) {
       global $config;
       return hash("sha256",$input.$config->salt);
    }
}