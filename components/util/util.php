<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name util.php - common unplaceable functions.
 */
 class util {
    static function shuffler($array) {
        $count = count($array)-1;
        for ($i = 0; $i <= $count; $i++) {
            $old = $array[$i];
            $n = mt_rand(0,$count);
            $array[$i] = $array[$n];
            $array[$n] = $old;
            //print " N:". $n ." I:". $i;
        }
        return $array;
    }

     static function ping($host,$port=80,$timeout=30)
     {
        $time = microtime();
        $fsock = @fsockopen($host, $port, $errno, $errstr, $timeout);
        if (!$fsock){
            return 0;
        }
        else
        {
            $t=(microtime() - $time)*1000;
            return round($t,2);
        }
    }

    /** Modified from barneygale https://gist.github.com/1235274 */
    static function pingWithExtra($host, $port=25565, $timeout=30) {
        $time = microtime();
    	//Set up our socket
        $fp = false;
    	if (!$fp = @fsockopen($host, $port, $errno, $errstr, $timeout)) return false;
        $time= ceil((microtime() - $time)*1000);

    	//Send 0xFE: Server list ping
    	fwrite($fp, "\xFE");

    	//Read as much data as we can (max packet size: 241 bytes)
    	$d = fread($fp, 256);

    	//Check we've got a 0xFF Disconnect
    	if (count($d)) {
            if(0 && $d[0] !== "\xFF") return false;
        }
        else {
            return false;
        }

    	//Remove the packet ident (0xFF) and the short containing the length of the string
    	$d = substr($d, 3);

    	//Decode UCS-2 string
    	$d = mb_convert_encoding($d, 'auto', 'UCS-2');

    	//Split into array
    	$d = explode("\xA7", $d);
        $data = count($d);
        //print $data;
    	//Return an associative array of values
        $array = array('time' => $time, 'motd' => $d[0], 'players' => "N",'max_players' => "A");
        if ($data >= 2) { $array['players'] = intval($d[1]); }
        if ($data >= 3) { $array['max_players'] = intval($d[2]); }
    	return $array;
    }

    static function BBCode($string) {
        $search = array(
        '@\[(?i)h(.*?)\](.*?)\[/(?i)h(.*?)\]@si',
        '@\[(?i)color=(.*?)\](.*?)\[/(?i)color\]@si',
        '@\[(?i)colour=(.*?)\](.*?)\[/(?i)colour\]@si',
        '@\[(?i)align=(.*?)\](.*?)\[/(?i)align\]@si',
        '@\[(?i)b\](.*?)\[/(?i)b\]@si',
        '@\[(?i)i\](.*?)\[/(?i)i\]@si',
        '@\[(?i)u\](.*?)\[/(?i)u\]@si',
        '@\[(?i)br\]@si',
        '@\[(?i)img (.*?),(.*?)\](.*?)\[/(?i)img\]@si',
        '@\[(?i)url=(.*?)\](.*?)\[/(?i)url\]@si',
        '@\[(?i)code\](.*?)\[/(?i)code\]@si',
        '@\[(?i)yt (.*?),(.*?)\](.*?)\[/(?i)yt\]@si',
        );
        $replace = array(
            '<h\\1>\\2</h\\1>',
            '<span style="color:\\1;">\\2</span>',
            '<span style="color:\\1;">\\2</span>',
            '<span style="text-align:\\1;">\\2</span>',
            '<b>\\1</b>',
            '<i>\\1</i>',
            '<u>\\1</u>',
            '<br/>',
            '<img width="\\1" height="\\2" src="\\3">',
            '<a href="\\1" target="_BLANK">\\2</a>',
            '<code>\\1</code>',
            '<iframe width="\\1" height="\\2" src="http://www.youtube.com/embed/\\3" frameborder="0"></iframe>'
        );
        return preg_replace($search, $replace, $string);
    }
 }
?>