<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name textinput.php
 */
  require_once("components/forms/input.php");
 class TextInput extends Input {
    protected $width;
    protected $height;
    protected $min;
    protected $max;
    protected $isPassword;
    protected $isTextArea;
    public function __construct($name,$value,$title,$id,$class,$script,$width,$height,$min,$max,$isPassword,$isTextArea) {
        global $mysql;
        parent::__construct($name,$value,$title,$id,$class,$script);
        $this->width = $width;
        $this->height = $height;
        $this->min = $min;
        $this->max = $max;
        $this->isPassword = $isPassword;
        $this->isTextArea = $isTextArea;
        $this->fail['regex'] = "Regex";
        $this->fail['min'] = str_replace("_"," ",$name) ." must be atleast $this->min characters long";
        $this->fail['max'] = str_replace("_"," ",$name) ." must not be longer than $this->max characters";
    }
    public function getMin() { return $this->min; }
    public function getMax() { return $this->max; }
    public function getIsPassword() { return $this->isPassword; }

    public function createHtml() {
        $html = "";
        if ($this->isTextArea) {
            $html = "<textarea name='$this->name' id='$this->id' cols='$this->width' rows='$this->height'  title='$this->title' $this->script>".stripslashes($this->value)."</textarea>";
        }
        else {
            $html = "<input name='$this->name' id='$this->id' maxlength='$this->max' size='$this->width' type='". ($this->isPassword ?  "password" : "text") ."' title='$this->title' value='". ($this->isPassword ?  "" : stripslashes($this->value)) ."' $this->script/>";
        }
        return $html;
    }
    public function validate($regex) {
        $length = strlen($this->value);
        if ($length >= $this->min)
        {
            if ($length <= $this->max)
            {
                if (!empty($regex)) {
                    if (!preg_match($regex, $this->value))
                    {
                        return true;
                    }
                    else
                    {
                        $this->failMsg = $this->fail['regex'];
                    }
                }
                else {
                    return true;
                }
            }
            else
            {
                $this->failMsg = $this->fail['max'];;
            }
        }
        else
        {
            $this->failMsg = $this->fail['min'];;
        }
        return false;
    }
 }



 ?>