<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name adminserverform.php
*/
require_once("components/forms/default/serverform.php");

class AdminServerForm extends ServerForm implements Form {
    public function __construct($server) {
        global $config;
        parent::__construct($server);
        $this->inputs['serverSponser'] = new CheckInput("Server_Sponser",$server->isSponsered(),"Is Sponsered","","checkInput","");
        $this->inputs['serverDelete'] = new CheckInput("Server_deleteCheck","","Delete Server From Listings",
        "deleteServer","checkInput","onClick='deleteServer();'");
    }

    /*
    *returns true when it fully vaildates.
    *else returns false if it fails or is displaying form.
    */
    public function Vaildate() {
        global $mysql;
        if (!empty($_POST['submit'])) {
            if ($this->inputs['serverDelete']->isChecked()) {
                Server::delete($this->server);
                print "<div>".$this->server->getName()." removed from listings.</div>";
                redirectHTMLtoReferer("index.php",4);
                return "delete";
            }
        }
        return parent::Vaildate();
    }
    public function showAdminInputsForm($page) {
        print "<tr>
            <td>
                Sponsered:
            </td>
            <td>".$this->inputs['serverSponser']->createHtml() ."</td>
        </tr>";
    }
    public function showDeleteForm($page) {
        print "<tr>
            <td>
                <div style='font-size: 0.8em;'>Delete Server from listings:</div>
                <div style='font-size: 0.7em;color:red;'>Warning! can not be undone.</div>
            </td>
            <td>".$this->inputs['serverDelete']->createHtml() ."</td>
            </tr>";
    }
     public function showForm($page,$continueShowingForm) {
        global $loggedInUser;
        if ($continueShowingForm || empty($_POST['submit']) || !empty($this->fail)) {
            $this->showBeginForm($page);
            $this->showInputs($page);
            $this->showVotifierForm($page);
            $this->showTagsForm($page);
            if ($loggedInUser->isAdmin()) {
                $this->showAdminInputsForm($page);
            }
            $this->showDeleteForm($page);
            $this->showEndForm($page);
        }
     }
	public function head() {
		parent::head();
		?>
        <script type='text/javascript'>
		var d = true;
		function deleteServer() {
			if (d) {
				alert("Are you sure you want to delete server from listings?");
			}
			d = !d;
		}
		</script>
        <?php
	}

     function UpdateServer() {
        global $mysql,$loggedInUser,$purifier;
        print "<strong>Updated!</strong>";
        $t = "";
        $count = count($this->tags);
        for ($i = 0; $i < $count; $i++) {
            if ($this->tags[$i]->isChecked()) {
                if ($i != 0) { $t .= ","; }
                $t .= $this->tags[$i]->getValue();
            }
        }
        if (!$this->tags[0]->isChecked()) { //remove the comma...
            $t = substr($t,1,strlen($t));
        }
        //(name,owner,logo,ip,port,description,sponsered,cookies,ping,tags)
        $sql = "UPDATE Servers SET
        name='".$this->inputs['serverName']->getValue()."',
        tags='".$t."',
        logo='". $this->inputs['serverLogo']->getValue() ."',
        ip='". $this->inputs['serverIp']->getValue() ."',
        port='". intval($this->inputs['serverPort']->getValue()) ."',
        website='". $this->inputs['serverWebsite']->getValue() ."',
        continent='". $this->inputs['serverContinent']->getValue() ."',
        unsafedescription='". $this->inputs['serverDescription']->getValue() ."',
        safedescription='". 
        //make sure we exscape this ;3
        $mysql->escape($purifier->purify($this->inputs['serverDescription']->getValue()))
         ."',
        comments='". intval($this->inputs['AllowComments']->isChecked()) ."',
        votifier='". intval($this->inputs['Votifier']->isChecked()) ."',
        votifierIp='". $this->inputs['VotifierIp']->getValue() ."',
        votifierPort='". intval($this->inputs['VotifierPort']->getValue()) ."',
        votifierRSAKey='". $this->inputs['VotifierRSAKey']->getValue() ."'
        WHERE id='". intval($this->server->getId()) ."'";
       // print $sql;
        $mysql->query($sql);
    }
 }
 ?>