<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name userform.php
*/
 require_once("components/forms/default/userform.php");
 require_once("components/forms/checkinput.php");
class AdminUserForm extends UserForm {
    public $fail = "";
    public $inputs = array(); // leave this public so we could add more?
    protected $user;
    public function __construct($user) {
        global $config;
        parent::__construct($user);
        $this->inputs['userName'] = null;
        $this->inputs['userDelete'] = new CheckInput("Delete_User","",
        "Delete User Account", "userDelete","checkInput","onClick='deleteUser();'");
        
        if ($user != null) { $v = $user->getTokens(); }
        if ($user != null && $user->isAdmin()) {
            $this->inputs['usersponser'] = new TextInput("User_Sponser",$v,
            "Sponsership tokens", "usersponser","textInput","",0,0,3,20,false,false);
        }
    }

    /*
    *returns true when it fully vaildates.
    *else returns false if it fails or is displaying form.
    */
    public function Vaildate() {
        if (!empty($_POST['submit'])) {
            /*
             * vaildate...
             */
             $success = true;
            if ($this->inputs['userDelete']->isChecked()) {
                User::delete($this->user->getId());
                print "<div>Account deleted</div>";
                redirectHTMLtoReferer("index.php",4);
                return "delete";
            }
            $this->inputs['userEmail']->setValue(filter_var($this->inputs['userEmail']->getValue(), FILTER_SANITIZE_EMAIL));
            if (!filter_var($this->inputs['userEmail']->getValue(), FILTER_VALIDATE_EMAIL)) {
                $success = false;
                $this->fail = "Invaild e-mail";
            }
            else if (!is_int(intval($this->inputs['usersponser']->getValue()))) {
                $success = false;
                $this->fail = "Invaild token amount, Must be an intger";
            }
            if ($this->inputs['userPass']->getValue()) {
                if (!$this->inputs['userPass']->validate("")) {
                    $success = false;
                    $this->fail = $this->inputs['userPass']->failMsg;
                }
                else if ($this->inputs['userPass']->getValue() != $this->inputs['userConfirmPass']->getValue()) {
                    $success = false;
                    $this->fail = "Passwords does not match";
                }
            }

        }
        return $success;
    }
   	public function head() {
		?>
      <script type='text/javascript'>
        var d = true;
        function deleteUser() {
            if (d) {
                alert("Are you sure you want to delete your user account and all server listings?");
            }
            d = !d;
        }
     </script>
     <?php
	}
    
    public function showBeginForm($page) {
        print "<div class='bubble register'>
            <form method='POST' action='$page'>
                <span style='color:red;'>$this->fail</span>
                <p>All fields with <span style='color:red;'>*</span> must be filled.</p>
                <table class='register'>\n";
    }
    public function showVotifierForm($page) {

    }
    public function showInputs($page) {
        global $loggedInUser;
        if ($loggedInUser != null && $loggedInUser->isAdmin()) {
            ?>
            <tr>
            <td><strong>Ip:</strong></td>
            <td><?php print $this->user->getIp(); ?></td>
            </tr>
            <tr>
            <td>Tokens:</td>
            <td><?php print $this->inputs['usersponser']->createHtml(); ?></td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td>Minecraft IGN</td>
            <td>
                <?php print $this->inputs['userIGN']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>E-mail:<span style='color:Red;'>*</span></td>
            <td>
            <?php print $this->inputs['userEmail']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>
                Password:<br />
                <span style='font-size: 0.8em;color:red;'>Do not use your<br />minecraft password!</span>
            </td>
            <td>
            <?php print $this->inputs['userPass']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>Confirm Password:</td>
            <td>
            <?php print $this->inputs['userConfirmPass']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>
                Delete Account:
                <div style='font-size: 0.7em;color:red;'>Warning! can not be undone.</div>
            </td>
            <td> <?php print $this->inputs['userDelete']->createHtml() ?></td>
        </tr>
    <?php
    }
    public function showEndForm($page) {
    ?>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2' style='margin:auto;'>
                            <input name='submit' type='submit' value='Submit!'/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    <?php
    }
     public function showForm($page,$continueShowingForm) {
        if ($continueShowingForm || empty($_POST['submit']) || !empty($this->fail)) {
            $this->showBeginForm($page);
            $this->showVotifierForm($page);
            $this->showInputs($page);
            $this->showEndForm($page);
        }
     }
 }
 ?>