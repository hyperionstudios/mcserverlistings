<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name input.php
 */
 class Input {
    public $fail = array();
    public $failMsg = "";
    protected $name;
    protected $id;
    protected $class;
    protected $value;
    protected $title;
    protected $script;
    public function __construct($name,$value,$title,$id,$class,$script) {
        if (isset($_POST[$name])) {
            $this->setValue($_POST[$name]);
        }
        else {
            $this->setValue($value);
        }
        $this->name = $name;
        $this->title = $title;
        $this->id = $id;
        $this->class = $class;
        $this->script = $script;
    }
    public function getName() { return $this->name; }
    public function getId() { return $this->id; }
    public function getClass() { return $this->class; }
    public function getValue() { return $this->value; }
    public function getTitle() { return $this->title; }

    public function setValue($value) {
        global $mysql;
        $this->value = $mysql->escape($value);
    }
    public function createHtml() {
        $html = "";
        return $html;
    }
    public function validate($regex) {
        return false;
    }
 }



 ?>