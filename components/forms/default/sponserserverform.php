<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name sponserserverform.php
 */
 require_once("components/forms/default/form.php");
 require_once("components/forms/textinput.php");
 require_once("components/forms/checkinput.php");
 require_once("components/forms/selectinput.php");
 require_once("components/var/server.php");
 require_once("components/util/serverutil.php");
 
 class SponserServerForm implements Form {
    public $fail = "";
    public $inputs = array(); // leave this public so we could add more?
    protected $user = null;

    protected $servers = array(); // This is just so i don't have to waste sql time....
    
    public function __construct($user, $servers) {
        global $config, $mysql;
        if ($user == null || $servers == null)
            return;
        $this->user = $user;
        $this->servers = $servers;
        $count = count($this->servers);
        for($i = 0; $i < $count; $i++) {
            $this->inputs[$i] = new CheckInput("Sponser_$i","",$this->servers[$i]->getName(),"","textInput","");
        }
    }

    /*
    *returns true when it fully vaildates.
    *else returns false if it fails or is displaying form.
    */
    public function Vaildate() {
        /*
         * vaildate...
         */
        $count = count($this->inputs);
        for($i = 0; $i < $count; $i++) {
            //One should be checked.
            if ($this->inputs[$i]->isChecked())
                return true;
        }
        $this->fail = "You require least one checked.";
        return false;
    }
   	public function head() {
		
	}
    public function showBeginForm($page) {
        print "<div class='bubble'>\n
            <form method='POST' action='$page'>\n
                <span style='color:red;'>$this->fail</span>\n
                <p>Check any server you wish to promote.</p>\n
                <table style='width:100%;'>\n";
    }
    public function showInputs($page) {
        $count = count($this->inputs);
        if ($count > 0) {
            for($i = 0; $i < $count; $i++) {
                print "<tr>\n<td>\n".$this->inputs[$i]->createHtml()."\n</td>\n<td>\n";
                serverUtil::simpleDisplayServer($this->servers[$i],false); // It "should" be the same index, right?
                print "</td>\n</tr>\n";
            }
        }
        else {
            print "<tr>\n<td>\nYou require to create a listing.\n</td>\n</tr>\n";
        }
    }
    public function showVotifierForm($page) {

    }

    public function showEndForm($page) {
    ?>  
                    <tr>
                        <td colspan='2' style='margin:auto;'>
                            <input name='submit' type='submit' value='Submit!'/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    <?php
    }
     public function showForm($page,$continueShowingForm) {
        if ($continueShowingForm || empty($_POST['submit']) || !empty($this->fail)) {
            $this->showBeginForm($page);
            $this->showInputs($page);
            //$this->showVotifierForm($page); //Ignore?
            $this->showEndForm($page);
        }
     }

 }

?>


