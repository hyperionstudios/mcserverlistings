<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name simplepageform.php - displays a page links thingy for large lists
 */

 class SimplePageForm {

    public static function displayForm($pages,$page,$min=4,$max=4) {
        if ($pages >= 1) {
            print "<span>Pages: ";
            $maxPages = ($page + $max);
            $i = ($page - $min);
            if ($i < 0) { $i = 0; }
            if ($i >= 1) { print SimplePageForm::getJSUrl(0,"&lt;&lt; "); }
            while ($i < $pages) {
                if ($i > $maxPages) { break; }
                if ($i == $page) {
                    print "&lt;=&gt;";
                }
                else {
                    $p = $i+1;
                    print SimplePageForm::getJSUrl($p);
                }
                $i++;
            }
            if ($maxPages <= $pages) { print SimplePageForm::getJSUrl(ceil($pages),"&gt;&gt; "); }
            ?>
            <form action='#'>
                <input type='text' id='pageIndex' size='5'/>
                <input type='button' value='Go' onclick='javascript:loadPage(document.getElementById("pageIndex").value,true);'/>
            </form>
            </span>
            <?php
        }
    }

    public static function getJSUrl($p,$v="") {
        if (!$v) { $v = $p; }
        return "<a href='javascript:loadPage($p,true);'>$v</a> ";
    }

 }
 ?>