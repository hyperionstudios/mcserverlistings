<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name form.php
*/
 require_once("components/forms/textinput.php");
 require_once("components/forms/checkinput.php");
 interface Form {
    public function Vaildate();
    public function head();
    public function showBeginForm($page);
    public function showInputs($page);
    public function showVotifierForm($page);
    public function showEndForm($page);
    public function showForm($page,$continueShowingForm);
 }
 ?>