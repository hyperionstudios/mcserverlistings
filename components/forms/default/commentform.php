<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name commentform.php
*/
 require_once("components/forms/default/form.php");
 require_once("components/forms/textinput.php");
 require_once("components/forms/checkinput.php");
 require_once("components/forms/selectinput.php");
 require_once("components/var/comment.php");

class CommentForm implements Form {
    public $fail = "";
    public $inputs = array(); // leave this public so we could add more?
    protected $simplePageForm;
    protected $comment = null;

    public function __construct() {
        global $config;
        $this->inputs['commentMSG'] = new TextInput("Comment_MSG","",
        "Enter your Comment", "commentMSG","textInput","",100,10,10,10000,false,true);
        if (!empty($_POST['Comment_MSG'])) {
            /** replace linebreaks with bbcode [br] */
            $this->inputs['commentMSG']->setValue(str_replace(array("\r\n", "\r", "\n"),"[br]",$_POST['Comment_MSG']));
        }
    }

    /*
    *returns true when it fully vaildates.
    *else returns false if it fails or is displaying form.
    */
    public function Vaildate() {
        if (!empty($_POST['submit'])) {
            /*
             * vaildate...
             */
            if (!$this->inputs['commentMSG']->validate("")) {
                $this->fail = $this->inputs['commentMSG']->failMsg;
            }
            else {
                return true;
            }
        }
        return false;
    }
    public function showBeginForm($page) {
        print "<div class='bubble postcomment'>
            <form method='POST' action='$page'>
            <span style='color:red;'>$this->fail</span>\n";
    }
    public function showInputs($page) {
        ?>
        <div style='margin:auto;'>
        <?php
        print $this->inputs['commentMSG']->createHtml();
        ?>
        </div>
        <?php
    }
    public function showVotifierForm($page) {

    }
    public function showEndForm($page) {
    ?>
                <input name='submit' type='submit' value='Submit!'/>
            </form>
        </div>
    <?php
    }
     public function showForm($page,$continueShowingForm) {
        if ($continueShowingForm || empty($_POST['submit']) || !empty($this->fail)) {
            $this->showBeginForm($page);
            $this->showInputs($page);
            $this->showVotifierForm($page);
            $this->showEndForm($page);
        }
     }

     public function head() {
        ?>
        <script type='text/javascript' src='template/Scripts/util.js'></script>
        <script type="text/javascript" src="template/Scripts/tiny_mce/tiny_mce.js"></script>
        <script type="text/javascript">
            tinyMCE.init({
                    theme : "advanced",
                    mode : "exact",
                    elements : "<?php print $this->inputs['commentMSG']->getName(); ?>",
                    plugins : "bbcode",
                    theme_advanced_buttons1 : "bold,italic,underline,hr,undo,redo,link,unlink,image,forecolor,styleselect,removeformat,cleanup,code",
                    theme_advanced_buttons2 : "",
                    theme_advanced_buttons3 : "",
                    theme_advanced_toolbar_location : "top",
                    theme_advanced_toolbar_align : "center",
                    theme_advanced_styles : "Code=codeStyle;Quote=quoteStyle",
                    content_css : "css/bbcode.css",
                    entity_encoding : "raw",
                    add_unload_trigger : false,
                    remove_linebreaks : false,
                    inline_styles : false,
                    convert_fonts_to_spans : false
            });
        </script>
        <?php
     }
 }
 ?>