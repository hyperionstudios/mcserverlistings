<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name userform.php
*/
 require_once("components/forms/default/form.php");
 require_once("components/forms/textinput.php");
 require_once("components/forms/checkinput.php");
 require_once("components/var/user.php");

class UserForm implements Form {
    public $fail = "";
    public $inputs = array(); // leave this public so we could add more?
    protected $user;
    public function __construct($user) {
        global $config;
        $v = "";
        if ($user != null) {
            $this->user = $user;
            $v = $user->getName();
        }
        $this->inputs['userName'] = new TextInput("UserName",$v,
        "Enter your desired user name.", "userName","textInput","",0,0,3,20,false,false);
        if ($user != null) { $v = $user->getIGN(); }
        $this->inputs['userIGN'] = new TextInput("User_IGN",$v,
        "Enter your Minecraft Game Name.", "userIGN","textInput","",0,0,3,50,false,false);
        if ($user != null) { $v = $user->getEmail(); }
        $this->inputs['userEmail'] = new TextInput("Email",$v,
        "Enter your email.", "userEmail","textInput","",0,0,0,500,false,false);
        $this->inputs['userPass'] = new TextInput("Password",$v,
        "Enter your password.", "userPass","textInput","",0,0,7,200,true,false);
        $this->inputs['userConfirmPass'] = new TextInput("Confirm_Password",$v,
        "Confirm Password.", "userConfirmPass","textInput","",0,0,7,200,true,false);
    }

    /*
    *returns true when it fully vaildates.
    *else returns false if it fails or is displaying form.
    */
    public function Vaildate() {
        if (!empty($_POST['submit'])) {
            /*
             * vaildate...
             */
             $success = true;
            if (!$this->inputs['userName']->validate("[^a-zA-Z0-9_]")) {
                $success = false;
                $this->fail = $this->inputs['userName']->failMsg;
            }
            else if (!filter_var($this->inputs['userEmail']->getValue(), FILTER_VALIDATE_EMAIL)) {
                $success = false;
                $this->fail = "Invaild e-mail";
            }
            else if (!$this->inputs['userPass']->validate("")) {
                $success = false;
                $this->fail = $this->inputs['userPass']->failMsg;
            }
            else if ($this->inputs['userPass']->getValue() != $this->inputs['userConfirmPass']->getValue()) {
                $success = false;
                $this->fail = "Passwords does not match";
            }
        }
        return $success;
    }
   	public function head() {
		
	}
    public function showBeginForm($page) {
        print "<div class='bubble register'>
            <form method='POST' action='$page'>
                <span style='color:red;'>$this->fail</span>
                <p>All fields with <span style='color:red;'>*</span> must be filled.</p>
                <table class='register'>\n";
    }
    public function showVotifierForm($page) {

    }
    public function showInputs($page) {
        ?>
        <tr>
            <td>UserName:<span style='color:Red;'>*</span></td>
            <td>
            <?php print $this->inputs['userName']->createHtml(); ?>
            </td>
        </tr>

        <tr>
            <td>Minecraft IGN</td>
            <td>
                <?php print $this->inputs['userIGN']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>E-mail:<span style='color:Red;'>*</span></td>
            <td>
            <?php print $this->inputs['userEmail']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>
                Password:<span style='color:Red;'>*</span><br />
                <span style='font-size:0.8em;color:red;'>Do not use your<br />minecraft password!</span>
            </td>
            <td>
            <?php print $this->inputs['userPass']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>Confirm Password:<span style='color:Red;'>*</span></td>
            <td>
            <?php print $this->inputs['userConfirmPass']->createHtml(); ?>
            </td>
        </tr>
    <?php
    }
    public function showEndForm($page) {
    ?>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2' style='margin:auto;'>
                            <input name='submit' type='submit' value='Submit!'/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    <?php
    }
     public function showForm($page,$continueShowingForm) {
        if ($continueShowingForm || empty($_POST['submit']) || !empty($this->fail)) {
            $this->showBeginForm($page);
            $this->showVotifierForm($page);
            $this->showInputs($page);
            $this->showEndForm($page);
        }
     }
 }
 ?>