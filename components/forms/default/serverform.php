<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name serverform.php
*/
 require_once("components/forms/default/form.php");
 require_once("components/forms/textinput.php");
 require_once("components/forms/checkinput.php");
 require_once("components/forms/selectinput.php");
 require_once("components/var/server.php");
 
class ServerForm implements Form {
    private $htmlConfig;
    
    public $fail = "";
    public $tags = array();
    public $inputs = array(); // leave this public so we could add more?
    protected $server = null;
    protected $port=25565;
    protected $logo="template/Styles/Images/minecraft-block-logo.png";

    public function __construct($server) {
        global $config;

        $v = "";
        if ($server != null) {
            $this->server = $server;
            $v = $server->getName();
        }
        $this->inputs['serverName'] = new TextInput("Server_Name",$v,
        "Enter your desired server name.", "serverName","textInput","",50,0,3,20,false,false);
        if ($server != null) { $v = $server->getIp(); }
        $this->inputs['serverIp'] = new TextInput("Server_Ip",$v,
        "Enter your server's ip.", "serverIp","textInput","",50,0,3,200,false,false);
        if ($server != null) { $v = $server->getPort(); }
        else { $v = $this->port; }
        $this->inputs['serverPort'] = new TextInput("Server_Port",$v,
        "Enter your server's port.", "servePort","textInput","",50,0,1,6,false,false,false);
        $v = "";
        if ($server != null) { $v = $server->getWebsite(); }
        $this->inputs['serverWebsite'] = new TextInput("Server_Website",$v,
        "Enter your server's website.", "serverWebsite","textInput","",50,0,0,200,false,false);

        if ($server != null) { $v = $server->getUnsafeDescription(); }
        $this->inputs['serverDescription'] = new TextInput("Server_Description",$v,
        "Enter your server's Description.", "serverDescription","textInput","",50,20,10,20000,false,true);

        if ($server != null) { $v = $server->getLogo(); }
        else { $v = $this->logo; }
        $this->inputs['serverLogo'] = new TextInput("Server_Logo",$v,
        "Enter your server's logo.", "serverLogo","textInput","",50,0,0,200,false,false);
        $v = "";
        if ($server != null) { $v = $server->getContinent(); }
        else { $v = $config->continents[0]; }
        $this->inputs['serverContinent'] = new SelectInput("Server_Continent",$v,
        "Enter your server's Continent.", "serverContinent","selectInput","",$config->continents);
        $v = "";
        if ($server != null) { $v = $server->allowComments(); }
        $this->inputs['AllowComments'] = new CheckInput("Server_AllowComments",$v,"Allow Comments?","AllowComments","textInput","");

        $v = "";
        if ($server != null) { $v = $server->hasVotifier(); }
        $this->inputs['Votifier'] = new CheckInput("Server_Votifier",$v,"Using Votifier?","Votifier","textInput","");
        if ($server != null) { $v = $server->getVotifierIp(); }
        $this->inputs['VotifierIp'] = new TextInput("Server_VotifierIp",$v,
        "Enter your server's Votifier ip.", "VotifierIp","textInput","",50,0,3,200,false,false);
        if ($server != null) { $v = $server->getVotifierPort(); }
        $this->inputs['VotifierPort'] = new TextInput("Server_VotifierPort",$v,
        "Enter your server's Votifier port.", "Votifierport","textInput","",50,0,1,6,false,false);
        if ($server != null) { $v = $server->getVotifierRSAKey(); }
        $this->inputs['VotifierRSAKey'] = new TextInput("Server_VotifierRSAKey",$v,
        "Enter your server's Votifier RSA key.", "VotifierRSAKey","textInput","",50,5,3,1000,false,true);

        $count = count($config->available_tags);
        $count2 = 0;
        $tags = null;
        if ($server != null) {
            $count2 = count($server->getTags());
            $tags = $server->getTags();
        }
        for ($i = 0; $i < $count; $i++) {
            $t = new CheckInput("tag[$i]","",$config->available_tags[$i],"tag[$i]","checkInput","");
            if (!empty($_POST['tag'][$i])) {
                $t->setValue($config->available_tags[$i]);
            }
            else {
                if (empty($_POST['submit'])) {
                    for ($ii = 0; $ii < $count2; $ii++) {
                        if ($tags[$ii] == $config->available_tags[$i]) {
                            $t->setValue($config->available_tags[$i]);
                            break;
                        }
                    }
                }
            }
            array_push($this->tags,$t);
        }
    }

    /*
    *returns true when it fully vaildates.
    *else returns false if it fails or is displaying form.
    */
    public function Vaildate() {
        if (!empty($_POST['submit'])) {
            /*
             * vaildate...
             */
            if (!$this->inputs['serverName']->validate("[^a-zA-Z0-9_]")) { 
                $this->fail = $this->inputs['serverName']->failMsg;
                return false;
            }
            else if (!$this->inputs['serverIp']->validate("")) {
                $this->fail = $this->inputs['serverIp']->failMsg;
                return false;
            }
            else if (!$this->inputs['serverPort']->validate("")) {
                $this->fail = $this->inputs['serverPort']->failMsg;
                return false;
            }
            else if (!is_numeric($this->inputs['serverPort']->getValue()) ||
                $this->inputs['serverPort']->getValue() < 1 ||
                $this->inputs['serverPort']->getValue() > 65535) {
                // extra validate of port...
                $this->fail = "Server port needs to be a integer between 1 and 65535";
                return false;
            }
            else if (!$this->inputs['serverWebsite']->validate("")) {
                $this->fail = $this->inputs['serverWebsite']->failMsg;
                return false;
            }
            else if (!$this->inputs['serverDescription']->validate("")) {
                $this->fail = $this->inputs['serverDescription']->failMsg;
                return false;
            }
            if (!$this->inputs['serverLogo']->validate("")) {
                if (!$this->inputs['serverLogo']->getValue()) {
                    $this->inputs['serverLogo']->setValue($this->logo);
                }
                else {
                    $this->fail = $this->inputs['serverLogo']->failMsg;
                    return false;
                }
            }

            $this->inputs['serverContinent']->validate("");

            /** Check to see if least one tag is checked */
            $count = count($this->tags);
            $c = false;
            for ($i = 0; $i < $count; $i++) {
                if ($this->tags[$i]->isChecked()) {
                    $c = true;
                    break;
                }
            }
            if (!$c) {
                $this->fail = "You require at least one tag.";
                return false;
            }
            return true;
        }
        return false;
    }
    public function showBeginForm($page) {
        print "<div class='bubble register serverregister'>
            <form method='POST' action='$page'>
                <span style='color:red;'>$this->fail</span>
                <p>All fields with <span style='color:red;'>*</span> must be filled.</p>
                <table class='register'>\n";
    }
    public function showInputs($page) {
        ?>
        <tr>
            <td>Server Name:<span style='color:Red;'>*</span></td>
            <td>
                <?php print $this->inputs['serverName']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>Website:</td>
            <td>
                <?php print $this->inputs['serverWebsite']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>Ip:<span style='color:Red;'>*</span></td>
            <td>
                <?php print $this->inputs['serverIp']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>Port:<span style='color:Red;'>*</span></td>
            <td>
                <?php print $this->inputs['serverPort']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>
                Logo:
                <a class='tooltip' href='#'>
                    <img src='template/Styles/Images/help-button.png' width='15'/>
                    <span>
                        <?php
                        $_GET['target'] = "banner";
                        include("help.php");
                        ?>
                    </span>
                </a>
            </td>
            <td>
                <?php print $this->inputs['serverLogo']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>Continent:</td>
            <td>
            <?php print $this->inputs['serverContinent']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>Allow Comments:</td>
            <td>
            <?php print $this->inputs['AllowComments']->createHtml(); ?>
        </td>
        </tr>
    <?php
    }
    public function showVotifierForm($page) {
        ?>
        <tr>
            <td>
                Use Votifier?:
                <a class='tooltip'>
                    <img src='template/Styles/Images/help-button.png' width='15'/>
                    <span>
                        <?php
                        $_GET['target'] = "votifier";
                        include("help.php");
                        ?>
                    </span>
                </a>
            </td>
            <td>
                <?php print $this->inputs['Votifier']->createHtml(); ?>

            </td>
        </tr>
        <tr>
            <td>Votifier ip:</td>
            <td>
            <?php print $this->inputs['VotifierIp']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>Votifier port:</td>
            <td>
            <?php print $this->inputs['VotifierPort']->createHtml(); ?>
            </td>
        </tr>
        <tr>
            <td>Votifier RSA key:</td>
            <td>
            <?php print $this->inputs['VotifierRSAKey']->createHtml(); ?>
            </td>
        </tr>
        <?php
    }

    public function showTagsForm($page) {
        global $config;
    ?>
    <tr>
        <td colspan='2'>
            <table cellspacing='0'>
                <tr>
                <?php
                $max = 3;
                $count = count($this->tags);
                print "<td rowspan='".ceil($count / $max)."'></th>Tags:<span style='color:Red;'>*</span>\n";
                ?>
                <a class='tooltip'>
                    <img src='template/Styles/Images/help-button.png' width='15'/>
                    <span>
                        <?php
                        $_GET['target'] = "tags";
                        include("help.php");
                        ?>
                    </span>
                </a>
                <?php
                $count = count($this->tags);
                for ($i = 0; $i < $count; $i++) {
                    if ($i != 0) {
                        if ($i % $max == 0) {
                            print "</tr>\n<tr>\n";
                        }
                        else {
                             print "<td style='background-color:#7F8488;width:1px;'></td>";
                        }
                    }
                    print "<td>".$config->available_tags[$i].":</td>
                    <td>".$this->tags[$i]->createHtml()."</td>";
                }
                ?>
                </tr>
            </table>
        </td>
    </tr>
    <?php
    }
    public function showEndForm($page) {
    ?>
                                <tr>
                                    <td>
                                        Description:<span style='color:Red;'>*</span>
                                        <a class='tooltip'>
                                            <img src='template/Styles/Images/help-button.png' width='15'/>
                                            <span>
                                            <?php
                                            $_GET['target'] = "desc";
                                            include("help.php");
                                            ?>
                                            </span>
                                        </a>
                                        <br />
                                        <span style='font-size:0.7em;'>HTML is disabled, use bbcode instead.</span>
                                    </td>
                                    <td>
                                        <?php print $this->inputs['serverDescription']->createHtml(); ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2' style='margin:auto;'>
                            <input name='submit' type='submit' value='Submit!'/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    <?php
    }
     public function showForm($page,$continueShowingForm) {
        if ($continueShowingForm || empty($_POST['submit']) || !empty($this->fail)) {
            $this->showBeginForm($page);
            $this->showInputs($page);
            $this->showVotifierForm($page);
            $this->showTagsForm($page);
            $this->showEndForm($page);
        }
     }

     public function head() {
        ?>
        <script type='text/javascript' src='template/Scripts/util.js'></script>
        <script type="text/javascript" src="template/Scripts/tiny_mce/tiny_mce.js"></script>
        <script type="text/javascript">
            tinyMCE.init({
                    theme : "advanced",
                    mode : "exact",
                    elements : "<?php print $this->inputs['serverDescription']->getName(); ?>",
                    theme_advanced_buttons1 : "bold,italic,underline,hr,undo,redo,link,unlink,image,forecolor,styleselect,removeformat,cleanup,code",
                    theme_advanced_buttons2 : "",
                    theme_advanced_buttons3 : "",
                    theme_advanced_toolbar_location : "top",
                    theme_advanced_toolbar_align : "center",
                    theme_advanced_styles : "Code=codeStyle;Quote=quoteStyle",
                    content_css : "css/bbcode.css",
                    entity_encoding : "raw",
                    add_unload_trigger : false,
                    remove_linebreaks : false,
                    inline_styles : false,
                    convert_fonts_to_spans : false
            });
        </script>
        <style type='text/css'>
            #serverDescription
            {
                width:100%;
            }
        </style>
        <?php
     }
 }
 ?>