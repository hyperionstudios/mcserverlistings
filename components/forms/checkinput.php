<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name textinput.php
 */
  require_once("components/forms/input.php");
 class CheckInput extends Input {

    public function __construct($name,$value,$title,$id,$class,$script) {
        parent::__construct($name,$value,$title,$id,$class,$script);
        if (!empty($_POST['submit'])) {
            if (empty($_POST[$name])) {
                $this->setValue("");
            }
        }
    }

    public function createHtml() {
        //if $value if empty, then it isn't checked
        $html = "<input type='checkbox' name='$this->name' id='$this->id' ".(empty($this->value) ? "" : "checked='1'")." title='$this->title' $this->script/>";
        return $html;
    }
    public function isChecked() {
        return !empty($this->value);
    }
 }



 ?>