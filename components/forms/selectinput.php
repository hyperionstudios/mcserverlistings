<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name selectinput.php
 */
  require_once("components/forms/input.php");
 class SelectInput extends Input {
    protected $items;
    public function __construct($name,$value,$title,$id,$class,$script,$items) {
        parent::__construct($name,$value,$title,$id,$class,$script);
        $this->items = $items;
    }
    public function vaildate($regex) {
        $count = count($this->items);
        for ($i = 0; $i < $count; $i++) {
            if ($this->items[$i] == $this->value) {
                return true;
            }
        }
        $this->setValue($this->items[0]);
        return false;
    }
    public function createHtml() {
        //if $value if empty, then it isn't checked
        $html = "<select name='$this->name' id='$this->id' class='$this->class'> $this->script/>";
        $count = count($this->items);
        for ($i = 0; $i < $count; $i++) {
            $html .= "<option value='". $this->items[$i] ."' ";
            if ($this->value == $this->items[$i]) { $html .= "selected='1' "; }
            $html .= ">". $this->items[$i] ."</option>\n";
        }
        $html .= "</select>";
        return $html;
    }
    public function isChecked() {
        return !empty($this->value);
    }
 }



 ?>