<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name config.php
 */

class config
{
    public $available_tags = array("PvP","Hardcore PvP","PvE","Anarchy","Creative","Survival","RolePlay",
    "Prison","White List","Vanilla","Bukkit","Tekkit","Custom","Offlinemode");
    public $continents = array("North America","Europe","Australia","South America","Asia","Africa","Antarctica");
    public $version = 1.7;

    public $salt = "NeedsToBeRandom!";
    
    //stuff about the site
    public $site_name = "Minecraft Server Listings";
    public $site_url = "";/*"http://127.0.0.1:8080";"http://www.mcserverlistings.com";*/
    public $site_root = "/";

    public $site_email = "admin@mcserverlistings.com";

    public $SmtpServer="127.0.0.1";
    public $SmtpPort="25"; //default
    public $SmtpUser="";
    public $SmtpPass="";

    //mysql
    
    public $mysql_host = "localhost";
    public $mysql_user = "";
    public $mysql_pass = "";
    public $mysql_database = "";

    //other settings
    public $sponsershipTime = 2592000; // 30days.
    
    /** note that "hoursUntilCanVoteAgain" is in seconds.*/
    public $hoursUntilCanVoteAgain = 86400; //1 day

    public $maxDisplaySponseredServers = 3;
    public $maxDisplayServers = 30;

    public $maxSponsers = 9;

    public $maxServersPerUser = 3;
    
    /* Paypal config */

    public $SandBox = false; //true;
  
    public $API_UserName="";
    public $API_Password="";
    public $API_Signature="";
    /**/
    //' example : $paymentAmount = "5.00";
    public $paymentAmount = "5.00";
    //' The currencyCodeType  
    public $currencyCodeType = "EUR";
    public $paymentType = "Sale";
    
    /*
    public $returnURL = "http://127.0.0.1:8080/paypalconfirm.php";
    public $cancelURL = "http://127.0.0.1:8080/paypalcancel.php";
    
    /* LIVE */
    public $returnURL = "http://www.mcserverlistings.com/paypalconfirm.php";
    public $cancelURL = "http://www.mcserverlistings.com/paypalcancel.php";
    /**/
    /** Cron config */
    public $ServersToUpdate = 100;
    public $maxTimeToUpdate = 5;
}
?>