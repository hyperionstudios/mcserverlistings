<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name about.php
 */
 require_once("template/template.php");

 $template = new template();

 $template->html_head("About");
 $template->html_body_aboveContent();

 ?>
 <h1>
    About
 </h1>
<div class='bubble'>
    <p>
        Minecraft Server Listings was created in late 2010, but due to technical issues
        was forced to close down for a revamp of the site. Now that it is late 2011 we
        are launching with a new look and a new dream of fair and accurate server
        listings.
    </p>
    <p>
        Our goal is to show you servers that are the best, and not just the
        richest. With our &#39;Cookie&#39; system, users may give their server a cookie once
        a day as a vote of confidence. The servers with the most cookies appear
        first on your list. We have implemented many feature to attempt to stop spamming of
        cookies as this gives some servers an unfair advantage.
    </p>
    <p>We support Votifier! :3</p>
    <p>
        Sponsored servers are special servers where owners donated to us
        (which not only pays for hosting and upgrades, but also the time to create and maintain it.)
        only <?php print $config->maxDisplaySponseredServers; ?> randomly will appear at the top
        and refreshes after a few minutes or so. They also get an eye catching &quotgold&quot color in the listings and server profile page.
    </p>
    <p>
        As we all know in
        the end it is all about you the player finding the best server possible that
        will fit your needs whether it is an anarchy server or a creative server, we have
        them all.
    </p>
    <div>
        Have a site related problem or some other related issue?<br/>
        Contact us at <a href='mailto:admin@mcserverlistings.com'>admin@mcserverlistings.com</a>
    </div>
    &nbsp;
    <div>
        <table style='text-align:left;margin:auto;'>
        <tbody>
        <tr>
            <td>Co-Owner:</td> 
            <td style='color:red;'>Jarvis</td>
        </tr>
        <tr>
            <td>Co-Owner &amp; Developer:</td>
            <td><a style='color:Olive;' href='mailto:admin@picklecraft.net'>Pickle</a></td>
        </tr>
        <tr>
            <td>Hosted by the almighty
            <td><a style='color:Green;font-size: medium;' href='http://orcworm.co.uk' ><b>Orcworm</b></a><td>!
        </tr>
        </tbody>
        </table>
    </div>
</div>

<?php
$template->html_body_belowContent();
$template->html_body_footer();
storeOldPage();
?>

