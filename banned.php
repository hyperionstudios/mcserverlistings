<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name about.php
 */
 require_once("template/template.php");

 $template = new template();

 $template->html_head("Banned");
 $template->html_body_aboveContent();
 if (!$blacklist) {
    redirectHTMLtoReferer();
 }
 else {
     ?>
     <h1>
        Access Denied
     </h1>
     <div class='bubble'>
        You were banned for: <span style='color:red;'><?php print $blacklist->getReason(); ?></span><br />
        If you think this is an mistake, contact us at <a href='mailto:admin@mcserverlisting.com'>admin@mcserverlisting.com</a>
     </div>
    
    
    <?php
}
$template->html_body_belowContent();
$template->html_body_footer();
?>

