<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name useradmin.php
 */
 require_once("template/template.php");
 require_once("components/var/user.php");
  require_once("components/var/server.php");
 require_once("components/forms/admin/userform.php");
 require_once("components/util/serverutil.php");
 $user = null;
 if (!empty($_GET['id'])) {
    $user = User::getUserFromId($_GET['id']);
 }
 $template = new template();
 $template->html_head("User Profile - ". ($user == null ? "" : htmlentities($user->getName())));

 $template->html_body_aboveContent();

 $success = false;
 if (isLoggedIn()) {
    if ($user != null) {
        if ($user->getId() == $loggedInUser->getId() || $loggedInUser->isAdmin()) {
            print "<h1>". (($user->getId() == $loggedInUser->getId()) ? "My" : htmlentities($user->getName())."'s") ." Profile</h1>";
            $userform = new AdminUserForm($user);
            $userform->head();
            $result = false;
            if (!empty($_POST['submit'])) {
                if ($result = $userform->Vaildate()) {
                   Update($userform);
                }
            }
            if ($result !== "delete") {
                $userform->showForm(getExecutingPage(),true);
                storeOldPage();
            }
            else {
               $sql = "SELECT * FROM servers WHERE owner='". $user->getId() ."'";
               $result = $mysql->query($sql);
               $servers = Server::serversFromResult($result);
               $count = count($servers);
               for ($i = 0; $i < $count; $i++) {
                    Server::delete($servers[$i]);
               }
               User::delete($user->getId());
               if ($user->getId() == $loggedInUser->getId()) {
                    session_destroy();
               }
               print "Deleted ". $user->getName();
            }

        }
        else {
            print "<h1>You do not have permisson.</h1>";
            redirectHTMLtoReferer("",0);
        }
    }
    else {
        print "<h1>User does not exist</h5>";
        redirectHTMLtoReferer("");
    }
}
else {
    redirectHTMLtoReferer("",0);
}
$template->html_body_belowContent();
$template->html_body_footer();

function Update($userform) {
    global $config,$mysql,$user;
    $ip = $_SERVER["REMOTE_ADDR"];
    print "<strong>Updated!</strong>";
    $sql = "UPDATE Users SET
    ip='$ip',
    email='". $userform->inputs['userEmail']->getValue() ."',
    IGN='". $userform->inputs['userIGN']->getValue() ."'";
    if ($userform->inputs['userPass']->getValue()) {
        require_once("components/util/encyrptor.php");
        $e = new encryptor($config);
        $password = $e->SHA256Hash($userform->inputs['userPass']->getValue());
        $sql .= ", password='$password', sponserTokens='".$userform->inputs['usersponser']->getValue()."'";
    }
    $sql .= " WHERE id='". $user->getId() ."'";
    //print $sql;
    $mysql->query($sql);
}

?>


