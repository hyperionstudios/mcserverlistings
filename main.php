<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name main.php - where everything is created
 */
require_once("config.php");
require_once("components/mysql.php");
require_once("components/var/blacklist.php");
require_once("components/var/user.php");
require_once("components/htmlpurifier/HTMLPurifier.auto.php");

session_start();
$config = new config();
$mysql = new mysql($config);

$loggedInUser = null;
$blacklist = null;

$htmlConfig = HTMLPurifier_Config::createDefault();
$htmlConfig->set('Filter.YouTube', true);
//$htmlConfig->set('HTML.Allowed', 'span[style],em,hr,p[style],strong,img[src|alt],b,a[href|title],i');
$htmlConfig->set('URI.MakeAbsolute', true);
$htmlConfig->set('AutoFormat.AutoParagraph', true);
$purifier = new HTMLPurifier($htmlConfig);

$ip = $_SERVER['REMOTE_ADDR'];
if (!$blacklist = BlacklistUser::getBlacklistFromIp($ip)) {
    if (!empty($_SESSION['user'])) {
        $loggedInUser = $_SESSION['user'];
    }
    else if (!empty($_COOKIE['user']) && !empty($_COOKIE['password'])) {
        $user = User::getUser($_COOKIE['user']);
        if ($user != null) {
            if ($user->isVerified()) {
                if ($user->login($_COOKIE['password'])) {
                    $loggedInUser = $user;
                }
            }
        }
    }
}
else {
    //Deny access.
   if ($_SERVER['PHP_SELF'] != "/banned.php") {
        header("Location: banned.php");
        exit;
   }
}
function logout() {
    global $loggedInUser;
    if ($loggedInUser != null) {
        session_destroy();
        $expire = time()-86400;
        setCookie("user","",$expire);
        setCookie("password","",$expire);
        return true;
    }
    return false;
}
function isLoggedIn() {
    global $loggedInUser;
    if ($loggedInUser != null) {
        return $loggedInUser->isLoggedIn();
    }
    return false;
}

function getExecutingPage() {
    $url = $_SERVER['SCRIPT_NAME'];
    if (!empty($_SERVER['QUERY_STRING'])) {
        $url .= "?". $_SERVER['QUERY_STRING'];
    }
    return  $url;
}

function storeOldPage() {
    $_SESSION['page'] = getExecutingPage();
}

function redirectHTMLtoReferer($url="index.php",$sec=4) {
    if (!empty($_SESSION['page']) && empty($url)) {
        $url = $_SESSION['page'];
    }
    else if (empty($url)) {
        $url = "index.php";
    }
    print "<meta http-equiv='REFRESH' content='$sec;url=$url'>
    <div>
        You should be auto redirected in about $sec seconds, if not click <a href='$url'>here</a>.
    </div>";
}
?>