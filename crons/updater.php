<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name updater.php - does updating, whatelse would it do.
*/
ini_set('display_errors',1);
error_reporting(E_ALL);
chdir("/var/www/mcserverlistings.com");
//chdir("C:\Users\Pickle\Dropbox\PHP\McServerlisting");
require_once("config.php");
require_once("components/mysql.php");
require_once("components/util/util.php");
require_once("components/var/server.php");
require_once("components/util/logostatus.php");
$config = new config();
$mysql = new mysql($config);
$mysql->connect();

$sql = "UPDATE Servers SET motd=?,playerslots=?,ping=?,lastcheck=?,downtime=?,uptime=?,formateduptime=? WHERE id=?";
$myStUpdate = $mysql->getLink()->prepare($sql) or die($mysql->getLink()->error);
$myStUpdate->bind_Param('ssiiiisi',$motd,$playerslots,$ping,$lastcheck,$downtime,$uptime,$formateduptime,$id);
            
$pages = Server::getServerCount() / 20;
for ($page = 0; $page <= $pages; $page++) {
    $servers = server::getLimitServers($page * 20,20);
    $count = count($servers);
    for ($i =0; $i < $count; $i++) {
        $time = time();
        $id = $servers[$i]->getId();
        $downtime = $servers[$i]->getDowntime();
        $uptime = $servers[$i]->getUptime();
        $lastcheck = $time;
        $ping = Util::pingWithExtra($servers[$i]->getIp(),$servers[$i]->getPort(),300);
        $motd = $servers[$i]->getMotd();
        $playerslots = "N/A";
        if ($ping) {
            $motd = $mysql->escape($ping['motd']);
            $playerslots = $mysql->escape($ping['players'] ."/". $ping['max_players']);
            $ping = $ping['time'];
            $uptime += floor(($time - $servers[$i]->getLastcheck()) / 60);
        }
        else {
            $downtime += floor(($time - $servers[$i]->getLastcheck()) / 60);
        }
        $formateduptime = Server::calcUptime($uptime,$downtime);    
            
        $myStUpdate->execute();
        
        LogoStatus::create(server::getServerByID($servers[$i]->getId()));
    }
}
//print "success";
?>