<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name serveradmin.php
*/
 require_once("template/template.php");
 $template = new template();

 $template->html_head("Terms of Services");
 $template->html_body_aboveContent();
?>
<h1>Terms of Services</h1>
<div class='bubble'>
    <h2>By using this service you agree to all the following terms.</h2>
    <br />
    <h3>Payments</h3>
    <p>
        All payments are nun refundable, unless there is techicnal failure with our system, we shall issue a refund.
        If for any reason you cancel your payment without good reason, you will be permantly banned from our site.
    </p>
    <h3>Images</h3>
    <p>
        All server logo are remotly hosted (except the default image).
        You must not link to anything illegal in the US or <strong>Germany</strong>,
        such as pornography, racist, anything related to nazism, or other obscene imagery.
        <br />
        Violation of this will result in a warning and immediate removal of said image.
        <br/>
        <br/>
        Also hotlinking or copying images hosted on this server is also prohibited.
    </p>
    <h3>Server Listing</h3>
    <p>
        By submiting a server to our listing, you agree to the following terms.
        <br/>
        You have permission from the sole owner of the <em>Minecraft Server</em> you are listing.
        If we find proof you are not the owner, we will place ownership to the correct person and you will be banned.
        <br />
        Also, you must provide accurate info about your server, ex: no links outside your <em>Minecraft Server</em>.
        This service will occasionally poll your server for ping time, MOTD, current players and max player slots.
        You may remove your server from our listings at any time.
        <br />
        You must not employ vote inflation fraud, or force users to vote.
        If we find evidence of such of this happening, you'll may be warned and vote count reseted, 
        or even possibly banned from our services.
        <br />
        We hold the right to remove any server from our listings without a reason.
        We will not remove a server listing for favoritism ,for bribes, etc.
    </p>
    <h3>User Accounts</h3>
    <p>
        By registering, you agree to the following terms.
        <br />
        You will not use your account to spam, post hateful comments, or other disturbence.
        We hold the right to remove any user from our system without a reason.
        Removal of an account removes all server listings posted with the account.
    </p>
    <div>
        We hold the right to change the ToS at anytime.<br/>
        Any questions? contact us <a href='mailto:<?php print $config->site_email; ?>'><?php print $config->site_email; ?></a>.
    </div>
</div>
<?php
 $template->html_body_belowContent();
 $template->html_body_footer();
 storeOldPage();
 ?>