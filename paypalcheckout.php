<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name paypalcheckout.php
*/
 //ini_set('display_errors',1);
 //error_reporting(E_ALL);
 //ob_start();
require_once("main.php");
if (isLoggedIn()) {
    $sql = "SELECT COUNT(*) FROM Servers WHERE owner='". $loggedInUser->getId() ."'";
    $result = $mysql->query($sql);
    $row = mysqli_fetch_array($result);
    $servers = $row['COUNT(*)'];
    if (count($servers) >= 1) {
    
        /* make sure our input is an integer. */
        $quantity = 1;
        if (!empty($_POST['quantity'])) { $quantity = intval($_POST['quantity']); }
        if (!is_int($quantity)) {
            $quantity = 1;
        }
        $_SESSION['quantity'] = $quantity;
        require_once("paypalfunctions.php");

        $PaymentOption = "PayPal";
        if ($PaymentOption == "PayPal") {
                // ==================================
                // PayPal Express Checkout Module
                // ==================================

                //'------------------------------------
                //' Calls the SetExpressCheckout API call
                //'
                //' The CallSetExpressCheckout function is defined in the file PayPalFunctions.php,
                //' it is included at the top of this file.
                //'-------------------------------------------------
                
                $items = array();
                $items[] = array('name' => 'Sponsership Token', 'amt' => $config->paymentAmount, 'qty' => $quantity);
            
                //::ITEMS::

                $resArray = SetExpressCheckoutDG($config->paymentAmount * $quantity, $config->currencyCodeType, $config->paymentType, 
                    $config->returnURL, $config->cancelURL, $items );

                $ack = strtoupper($resArray["ACK"]);
                if($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
                        $token = urldecode($resArray["TOKEN"]);
                         RedirectToPayPalDG($token);
                } 
                else {
                        //Display a user friendly Error on the page using any of the following error information returned by PayPal
                        $ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
                        $ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
                        $ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
                        $ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);
                        
                        echo "SetExpressCheckout API call failed. ";
                        echo "Detailed Error Message: " . $ErrorLongMsg;
                        echo "Short Error Message: " . $ErrorShortMsg;
                        echo "Error Code: " . $ErrorCode;
                        echo "Error Severity Code: " . $ErrorSeverityCode;
                }
        }
    }
    else {
        print "<h3>You require to register a server.</h3>";
        redirectHTMLtoReferer("registerserver.php",5);
    }
}
else {
	print "<h3>You may want to log in before you pay. :)</h3>";
    redirectHTMLtoReferer("login.php",5);
}
?>
