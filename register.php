<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name register.php
 */
 require_once("template/template.php");
 require_once("components/forms/default/userform.php");
 require_once("components/var/blacklist.php");
 $template = new template();
 $template->html_head("Register");
 $template->html_body_aboveContent();
if (!empty($_GET['resend'])) {
    if (isLoggedIn()) {
        User::emailVerification($loggedInUser->getName(),$loggedInUser->getIGN(),$loggedInUser->getEmail(),$loggedInUser->getVerifyKey());
            print "<div>
            <h1>Email Sent</h1>
            Check your email and verify it.<br/>
            <a href='register.php?resend=1'>Resend Confirmation Email</a>
          </div>";
    }
}
else {
    if ($blacklist = BlacklistUser::getBlacklistFromIp($ip)) {
        print "<div style='color:red'>
        <h1>You have been banned from this site.</h1>
        Reason: ". $blacklist->getReason() ."
        </div>";
    }
    else {
        $success = false;
        $userform = new UserForm(null);
        if (!empty($_POST['submit'])) {
            if ($result = $userform->Vaildate()) {
                Register($userform);
            }
        }
        if (!$success) {
            print "<h1>Register an account!</h1>";
            $userform->showForm(getExecutingPage(),true);
        }
    }
}
$template->html_body_belowContent();
$template->html_body_footer();

function Register($userform) {
    global $config, $mysql, $success,$ip;
    $success = true;
    $username = $userform->inputs['userName']->getValue();
    $email = strtolower($userform->inputs['userEmail']->getValue());
    $ign = strtolower($userform->inputs['userIGN']->getValue());
    $sql = "SELECT * FROM Users WHERE name='$username' OR IGN='$ign' OR email='$email' OR ip='$ip'";
    $result = $mysql->query($sql);
    while ($rows = $result->fetch_array()) {
        if (strtolower($username) == strtolower($rows['name'])) {
            $userform->fail = "Username already exists";
            $success = false;
            break;
        }
        else if ($email == strtolower($rows['email'])) {
            $userform->fail = "An account already exists with that email.";
            $success = false;
            break;
        }
        else if ($ip == $rows['ip']) {
            $userform->fail = "You already got an account.";
            $success = false;
            break;
        }
        else if ($ign == $rows['IGN'] && !empty($ign)) {
            $userform->fail = "That IGN is in use...";
            $success = false;
            break;
        }
    }
    if ($success) {
       success($userform,$ip);
    }
}

function success($userform,$ip) {
    User::createUser($userform->inputs['userName']->getValue(), $userform->inputs['userIGN']->getValue(),
    $userform->inputs['userPass']->getValue(),$userform->inputs['userEmail']->getValue(), $ip);
    $user = User::getUser($userform->inputs['userName']->getValue());
    User::emailVerification($user->getName(),$user->getIGN(),$user->getEmail(),$user->getVerifyKey());
    login($userform->inputs['userName']->getValue(),$userform->inputs['userPass']->getValue(),$user);

    print "<div>
            <h1>You are now registered!</h1>
            Check your email and verify it.<br/>
            <a href='register.php?resend=1'>Resend Confirmation Email</a>
          </div>";
}

function login($username,$pass,$user) {
    global $mysql,$config;
	if ($user != null) {
	    if ($user->login($pass)) {
          $_SESSION['user'] = $user;
 	      return true;
        }
        else {
            $fail = "Incorrect password.";
        }
	}
	else {
	 $fail = "Username does not exist.";
	}
    return false;
 }
?>


