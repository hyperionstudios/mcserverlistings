<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name sponsermanager.php
 */
 require_once("template/template.php");
 require_once("components/var/server.php");
 require_once("components/var/blacklist.php");
 require_once("components/forms/default/sponserserverform.php");
 
 $user = null;
 $servers = null;
 $sponserServerCount = 0;
 if (!empty($_GET['id'])) {
    if (isLoggedIn()) {
        if ($loggedInUser->getId() == $_GET['id']) {
            $user = $loggedInUser;
        }
        else {
            $user = User::getUserFromId($_GET['id']);
        }
    }
 }
 else {
    if (isLoggedIn())
        $user = $loggedInUser;
 }
 $template = new template();
 $template->html_head("Sponser Manager");
 $template->html_body_aboveContent();

 if (isLoggedIn()) {
    if ($user != null) {
        if ($blacklist = BlacklistUser::getBlacklistFromIp($ip)) {
            //We dislike his/her kind.
            print "<div style='color:red'>\n
            <h1>You have been banned from this site.</h1>\n
            Reason: ". $blacklist->getReason() ."\n
            </div>\n";
        }
        else {
            $sponserServerCount = Server::getSponseredServerCount(true);
            
            $sql = "SELECT * FROM Servers WHERE owner='". $user->getId() ."'";
            $result = $mysql->query($sql);
            $servers = Server::serversFromResult($result);
            
            $sponserServerForm = new SponserServerForm($user,$servers);
            
            if (!empty($_POST['submit'])) {
                if ($sponserServerForm->Vaildate()) {
                   giveServersSponser($sponserServerForm); // lets go sort this, yes?
                }
            }
            showDashBoard();
            if ($sponserServerCount >= $config->maxSponsers) {
                print "<div>Sorry, but at this moment, we reached our max quota sponsership. Please wait untill one is opened.</div>";
            }
            $sponserServerForm->showForm(getExecutingPage(),true);
            storeOldPage();
        }
    }
    else {
        print "<h1>User does not exist</h5>";
        redirectHTMLtoReferer("");
    }
}
else {
    redirectHTMLtoReferer("login.php",0);
}
$template->html_body_belowContent();
$template->html_body_footer();

function showDashBoard() {
    global $mysql, $loggedInUser,$user;
    if ($user->getId() == $loggedInUser->getId() || $loggedInUser->isAdmin()) {
       ?>
       <div class='bubble' style='text-align:center;'>
            <div>
                You currently have <u><strong><?php print $user->getTokens(); ?></strong></u> Tokens.<br/>
                Visit the <a href='premium.php'>Premium</a> page to buy more.<br/>
            </div>
            <p>
                Sponsership is stackable, giving one listing a token increases the time it is run, blocking out the competion.
                You will not be able to use or buy a token once all 9 slots are full, you must wait untill one is open.
            </p>
        </div>   
        <?php
    }
}
function giveServersSponser($sponserServerForm) {
    global $mysql,$config, $servers, $user, $sponserServerCount;
    if ($sponserServerCount >= $config->maxSponsers) {
        /* reached max sponsers. */
        return;
    }
    $leastOneSucceed = false;
    //get how many are checked.
    $count = count($sponserServerForm->inputs);
    if ($count > 0) {
        for($i = 0; $i < $count; $i++) {
            
            if ($sponserServerCount >= $config->maxSponsers) {
                $sponserServerForm->fail .= "We reached our max quota sponsership";
                break;
            }
            if ($sponserServerForm->inputs[$i]->isChecked()) {
                if (!$user->useToken()) {
                    $sponserServerForm->fail .= "<br/>\nYou do not have enough tokens!\n";
                    break;
                }
                else {
                    $leastOneSucceed = true;
                    if (!$servers[$i]->isSponsered())
                        $sponserServerCount++;
                    $servers[$i]->giveSponsership();
                    
                }
            }            
        }
    }
    if ($leastOneSucceed) {
        print "<h5>Congrats, you have used a sponser token!</h5>\n";
    }

}

?>


