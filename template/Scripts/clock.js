/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name clock.js -auto uppdateing clock...
 */
var montharray=new Array("January","February","March","April","May","June",
                "July","August","September","October","November","December");
var date;
var year;
var month;
var day;
var hours;
var min;
var sec;
var med = "AM";
var toggle = false;
var id;
function startClock(i,s) {
    id = i;
    date = new Date(s);
    year = date.getFullYear();
    month = date.getMonth();
    day = date.getDate();
    hours = date.getHours();
    min = date.getMinutes();
    sec = date.getSeconds();
    clock();
}
function clock() {
    if (++sec >= 60) {
        sec = 0;
        if (++min >= 60) {
            min = 0;
            ++hours;
        }
    }
    if (hours >= 12) {
        if (hours >= 13) {
            hours -= 12;
        }
        if (hours == 12 && !toggle) toggle = !toggle;
        if (hours == 0 && toggle) toggle = !toggle;
        
        med = toggle ? "PM" : "AM";
    }
    var t = montharray[month] +" "+ fixTime(day) +","+ year+" "+ hours +":"+ fixTime(min) +":"+ fixTime(sec) +" "+ med;
    document.getElementById(id).innerHTML=t;
    setTimeout('clock()',1000);
}
function fixTime(t) {
    if (t < 10) { t = "0"+t; }
    return t;
}