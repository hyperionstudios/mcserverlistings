/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name updateServer.js - ajax shit...
 */
var searchFilters = new Array();
var page;
var name;
var continent;
//<!--
// This is some ajax to rotate the sponser list.
// TODO: fix this where it shall only change the values
// instead of retreving all the html....
// seems like a waste of bandwidth.
// Look into JSON
// -->
function getServers(id,code,page,name,continent,useFilters) {
    this.page = page;
    this.name = name;
    this.continent = continent;
    var xmlhttp;
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById(id).innerHTML=xmlhttp.responseText;
        }
      }
      var url = "servers.php?js="+code;
      if (useFilters) {
        var filters = "";
        for(i =0; i < searchFilters.length; i++) {
            filters = filters+searchFilters[i]+",";
        }
        if (filters.length != 0) {
            filters = filters.substr(0,filters.length-1);
            url += "&filters="+ filters;
        }
      }
      if (page != undefined) {
        url += "&page="+page;
      }
      if (name != undefined) {
        url += "&name="+name;
      }
      if (continent != undefined) {
        url += "&continent="+continent;
      }
     //alert(url);
    xmlhttp.open("GET",url,true);
    xmlhttp.send();
}
function loadPage(page,usefilters) {
    getServers("Servers","SH234ABg",page,this.name,this.continent,true);
}
function searchName(name,page,usefilters) {
    getServers("Servers","SH234ABg",page,name,this.continent,true);
}
function SortContinents(continent,page,usefilters) {
    getServers("Servers","SH234ABg",page,this.name,continent,true);
}
function toggleFilter(tag) {
    var contains = false;
    var element = document.getElementById(tag.replace(" ","_"));
    for(i =0; i < searchFilters.length; i++) {
       if (searchFilters[i] == tag) {
            contains = true;
            searchFilters.splice(i,1);
            if (element != undefined) {
                element.className = "button";
            }
            break;
       }
    }
    if (!contains) {
        searchFilters.push(tag);
        if (element != undefined) {
                element.className = "button activebutton";
        }
    }
    getServers("Servers","SH234ABg",0,this.name,this.continent,true);
}