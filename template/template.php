<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name template.php - the display page
 */
//ini_set('display_errors',1);
//error_reporting(E_ALL);
require_once("./main.php");

class template {
    private static $timeGenerated = 0;
    
    public function __construct() {
       Template::getTimeGenerated();
    }
    public static function getTimeGenerated() {
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        if (Template::$timeGenerated == 0) {
            Template::$timeGenerated = $time;
        }
        else {
            return round(($time - Template::$timeGenerated), 4);
        }
        return 0;
    }
    
    public function html_head($title="") {
    global $mysql,$config;
    
    ?>
    <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en'>
    <head id='head'>
        <title><?php print "$title - $config->site_name"; ?></title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name='description' content='Our goal is to help you select the best server,
        and we list hundred of free to use servers. Come Check Us Out!' />
        <meta name='keywords' content='minecraft servers,minecraft server list,
        minecraft servers list,minecraft servers ip,server listings,
        free minecraft servers,best minecraft server,minecraft servers ip list,
        minecraft server ips,VoteForDiamonds' />
        <meta name='ROBOTS' content='INDEX, FOLLOW, ARCHIVE' />
        <meta property='og:image' content='<?php print $config->site_url; ?>template/Styles/Images/minecraft-block-logo.png'/>
        <meta name='google-translate-customization' content='64a7e486ae0113de-51ad06e87c033808-g45a65949e2afd56f-18'/>
        <link rel='icon' type='image/png' href='<?php print $config->site_url; ?>/template/favicon.png' />
        <link rel='icon' type='image/x-icon' href='<?php print $config->site_url; ?>/template/favicon.ico' />
        <link href='<?php print $config->site_url; ?>/template/Styles/Site.css' rel='stylesheet' type='text/css' />
        <!-- Google -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-27925532-1']);
            _gaq.push(['_trackPageview']);

            (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
        <script type="text/javascript">
          (function() {
            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
          })();
        </script>
        <script type='text/javascript' src='<?php print $config->site_url; ?>/template/Scripts/clock.js'></script>
    <?php
        flush();
    }
    public function html_body_aboveContent() {
    global $config, $mysql, $loggedInUser;
    ?>
    </head>
    <body onload='startClock("clock",<?php print time() * 1000; ?>);'>
        <!-- Project Wonderful Ad Box Loader -->
        <!-- Put this after the <body> tag at the top of your page -->
        <script type="text/javascript">
           (function(){function pw_load(){
              if(arguments.callee.z)return;else arguments.callee.z=true;
              var d=document;var s=d.createElement('script');
              var x=d.getElementsByTagName('script')[0];
              s.type='text/javascript';s.async=true;
              s.src='//www.projectwonderful.com/pwa.js';
              x.parentNode.insertBefore(s,x);}
           if (window.attachEvent){
            window.attachEvent('DOMContentLoaded',pw_load);
            window.attachEvent('onload',pw_load);}
           else{
            window.addEventListener('DOMContentLoaded',pw_load,false);
            window.addEventListener('load',pw_load,false);}})();
        </script>
        <!-- End Project Wonderful Ad Box Loader -->

        <div class='header'>
                <div class='title'>
                    <a href='<?php print $config->site_url; ?>'>
                        <img style='margin:5px 0 0 10px;' src='<?php print $config->site_url; ?>/template/Styles/Images/logo-banner.png' alt='<?php print $config->site_name; ?>Minecraft Servers, Minecraft Server List'/>
                    </a>
                </div>
                <div class='loginDisplay'>
                    <?php
                    if (isLoggedIn()) {
                        print "<div id='user'>
                            Logged in as <strong>
                            <a href='userprofile.php?id=". $loggedInUser->getId() ."'>
                            ". $loggedInUser->getName() ."
                            </a></strong><br/>
                            [<strong><a href='$config->site_url/logout.php'>Logout</a></strong>]
                        </div>";
                    }
                    else {
                        print "<div>
                            [<a href='$config->site_url/login.php'>Login</a>]
                            [<a href='$config->site_url/register.php'>Register</a>]
                        </div>";
                    }
                    ?>
                </div>
                <div>                    
                    <div class='social'>
                        <a href='http://twitter.com/McServListings' title='Follow us on our Twitter' target='_blank'>
                        <img src='<?php print $config->site_url; ?>/template/Styles/Images/social/twitter.png' alt='Twitter' />
                        </a>
                    </div>                    
                    <div class='social'>
                        <a href='http://plus.google.com/102930729651398506436' title='Follow us on our G+ page!' target='_blank'>
                        <img src='<?php print $config->site_url; ?>/template/Styles/Images/social/google.png' alt='Google Plus' />
                        </a>
                    </div>
                    <div class='social'>
                        <a href='http://www.facebook.com/McServerListings' title='Follow us on our Facebook page!' target='_blank'>
                            <img src='<?php print $config->site_url; ?>/template/Styles/Images/social/facebook.png' alt='Facebook' />
                        </a>
                    </div>


                </div>
                <div class='clear hideSkiplink'>
                    <div class='menu'>
                        <ul>
                            <li>
                                <a href='<?php print $config->site_url; ?>/registerserver.php'>Submit a Minecraft Server</a>
                            </li>
                            <li>
                            <a href='<?php print $config->site_url; ?>/premium.php'>Premium</a>
                            </li>                          
                            <li>
                                <a href='<?php print $config->site_url; ?>/about.php'>About</a>
                            </li>
                            <?php
                            if (isLoggedIn()) {
                                    ?>
                                    <li>
                                    <a href='<?php print $config->site_url; ?>/userprofile.php?id=<?php print $loggedInUser->getId(); ?>'>My Profile</a>
                                    </li>
                                    <?php
                                if ($loggedInUser->isAdmin()) {
                                    ?>
                                    <li>
                                    <a href='<?php print $config->site_url; ?>/admin/admin.php'>Admin</a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>                           
                    </div>
                    <div style='color: #EFFECD; float:right;margin:15px 15px 0 0;' id='clock'>&nbsp;</div>   
                    <div class='clear'></div>
                </div>

                </div>
            <div class='page'>
            <div class='adCol leftCol'>
                <script type='text/javascript'><!--
                    google_ad_client = 'ca-pub-0385017422927470';
                    /* mcservers */
                    google_ad_slot = '1740965901';
                    google_ad_width = 120;
                    google_ad_height = 600;
                  //-->
                </script>
                <script type='text/javascript'
                src='http://pagead2.googlesyndication.com/pagead/show_ads.js'>
                </script>
            </div>
            <div class='adCol rightCol'>
                <script type='text/javascript'><!--
                    google_ad_client = 'ca-pub-0385017422927470';
                    /* mcservers */
                    google_ad_slot = '1740965901';
                    google_ad_width = 120;
                    google_ad_height = 600;
                    //-->
                </script>
                <script type='text/javascript'
                src='http://pagead2.googlesyndication.com/pagead/show_ads.js'>
                </script>
            </div>
            <div class='upperAds'>
                <!-- Project Wonderful Ad Box Code -->
                <div style="text-align:center;"><div style="display:inline-block;" id="pw_adbox_61372_1_0"></div></div>
                <!-- End Project Wonderful Ad Box Code -->
            </div>
            <div class='content'>
        <?php
            flush();
        }
        public function html_body_belowContent() {
        global $config;
        global $mysql;
        ?>
            </div>
            <div class='clear'>
            </div>
        <?php
            flush();
        }
        public function html_body_footer() {
        global $config, $mysql;
        ?>
        <div class='footer'>
            <div style='float:left;margin:2px 18px'>
				<a href='http://s10.flagcounter.com/more/eeR'>
				<img src='http://s10.flagcounter.com/count/eeR/bg_E6E6E6/txt_000000/border_E6E6E6/columns_4/maxflags_8/viewers_0/labels_0/pageviews_1/flags_0/' alt='Free counters!'/>
				</a>
            </div>
            <div style='margin:0 20%;'>
                Copyright &copy; 2012 - all rights reserved, we are not affiliated with Mojang in anyway.<br/>
                Design by: Pickle - best viewed with IE9 or Opera 12.<br/>
                <a href='tos.php' title='Terms of Services'>Terms of Service</a> | <a href='privacy.php'>Privacy Policy</a>
                <br />
                Page generated in <?php print Template::getTimeGenerated(); ?>ms.
            </div> 
            <div id='google_translate_element' style='float: right;'></div>
            <script type='text/javascript'>
            function googleTranslateElementInit() {
              new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_RIGHT, gaTrack: true, gaId: 'UA-27925532-1'}, 'google_translate_element');
            }
            </script><script type='text/javascript' src='//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit'></script>

        </div>
        </body>
        </html>
        <?php
            flush();
        }
    }
?>