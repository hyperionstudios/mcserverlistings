<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name servers.php
 */
 require_once("template/template.php");
 require_once("components/var/server.php");
 require_once("components/util/serverutil.php");
 require_once("components/forms/default/simplepageform.php");
  /**
 * This is for when the client requests to update the sponser list.
 */
 if (!empty($_GET['js'])) {
     if ($_GET['js'] == "SH234ABs") {
        displayRandomSponsers();
     }
     else if ($_GET['js'] == "SH234ABg") {
        if (!empty($_GET['filters'])) {
            displayServers(explode(",",$mysql->escape($_GET['filters'])));
        }
        else {
            displayServers(array());
        }
     }
 }
 else {
 $template = new template();

 $template->html_head("Server List");
 ?>
 <script type='text/javascript' src='template/Scripts/updateServer.js'>
    setInterval('getServers("SponserServers","SH234ABs",false)',90000);
 </script>
 <script type='text/javascript' src='template/Scripts/util.js'></script>
 <?php
 $template->html_body_aboveContent();
 ?>
 <h1>Sponsered Minecraft Servers</h1>
 <div class='bubble' id='SponserServers'> 
<?php
    print "Servers submitted: ".Server::getNextServerId();
    displayRandomSponsers();
?>
9 minecraft servers are randomly displayed here. Want your minecraft server to be one of them??<br/>
Then go here to upgrade today!
<a href='premium.php'>Become Sponsered</a>.
</div>
<h2>Minecraft Servers</h2>
    <div class='bubble'>
        Search Filters for The Minecraft Servers
        <a class='tooltip'>
            <img src='template/Styles/Images/help-button.png' width='15'/>
            <span>
                <?php
                $_GET['target'] = "tags";
                include("help.php");
                ?>
            </span>
        </a>
        <table>
        <tr>
    <?php
    $count = count($config->available_tags);
    for ($i = 0; $i < $count; $i++) {
        if ($i != 0 && ($i % 7) == 0) { print "</tr><tr>"; }
        createButton($config->available_tags[$i]);
    }
    createButton("Online Status");
    print "</tr>
    <tr>
        <td colspan='3'>
            <select name='continent' onchange='SortContinents(this.value,0,true)'>";
                $count = count($config->continents);
                for ($i = 0; $i < $count; $i++) {
                    print "<option value='".$config->continents[$i]."'>".$config->continents[$i]."</option>";
                }
        print "</select>
        </td>
        <td colspan='4'>Search: <input type='text' onkeypress='searchName(this.value,0,true);'/></td>
    </tr>
    </table>
    <div id='Servers'>";
        displayServers(array());
    print "<div>note minecraft servers that have over 300ms ping from this server will show up as \"offline\".</div>";
print "</div>";

$template->html_body_belowContent();
$template->html_body_footer();
 storeOldPage();
}

function createButton($value) {
    print "<td class='button' id='".str_replace(" ","_",$value) ."'>
        <a href='javascript:toggleFilter(\"$value\");'>
            $value
        </a>
    </td>\n";
}

function displayRandomSponsers() {
    global $config;
    $servers = Server::getNonSqlRandSponseredServers($config->maxDisplaySponseredServers);
    $count = count($servers);
    for ($i = 0; $i < $count; $i++) {
        serverUtil::simpleDisplayServer($servers[$i],true);
    }
    if ($count < 1) {
        print "Currently No Sponsers. :(";
    }
 }
 function displayServers($filters) {
    global $config,$mysql,$loggedInUser;
    $page = 0;
    $pages = 0;
    if (!empty($_GET['page'])) {
        $page = $mysql->escape($_GET['page']) - 1;
    }
    $sql = "SELECT * FROM Servers ";
    $count = count($filters);
    for ($i = 0; $i < $count; $i++) {
        if ($filters[$i] === "Online Status") {
            if ($i == 0) {
                $sql .= "WHERE ping != 0";
            }
            else {
                $sql .= "AND ping != 0";
            }
        }
        else {
            if ($i == 0) {
                $sql .= "WHERE tags LIKE '%$filters[$i]%'";
            }
            else {
                $sql .= " AND tags LIKE '%$filters[$i]%'";
            }
        }
    }
    if (!empty($_GET['name'])) {
        $name = $mysql->escape($_GET['name']);
        if ($count < 1) { $sql .= "WHERE name LIKE '%$name%'"; }
        else { $sql .= " AND name LIKE '%$name%'"; }
    }
    $countSql = $sql;

    $orderbySql = " ORDER BY ping = '0' ASC,";
    if (!empty($_GET['continent'])) {
        $continent = $mysql->escape($_GET['continent']);
        $orderbySql .= " continent = '$continent' DESC,";
    }
    $orderbySql .= " cookies DESC";
    $sql .= $orderbySql ." LIMIT ".($page*$config->maxDisplayServers).", ". $config->maxDisplayServers;

    if ($result = $mysql->query($sql)) {
        $servers = Server::serversFromResult($result);
        $count = count($servers);
        for ($i = 0; $i < $count; $i++) {
            serverUtil::simpleDisplayServer($servers[$i],true);
        }
    }
    if ($count < 1) {
        print "<h2>Can not find servers.. Maybe try looking with other filters?</h2>";
    }
    /**
     * handle the page form thingy
     */
    $countSql = str_replace("*","COUNT(*)",$countSql);
    $r = $mysql->query($countSql);
    $row = mysqli_fetch_array($r);
    $y = $row['COUNT(*)'];
    SimplePageForm::displayForm($y / $config->maxDisplayServers,$page,4,4);
    print "</div>";
 }



?>