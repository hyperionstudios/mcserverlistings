<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name lostpassword.php
*/
 require_once("template/template.php");
 require_once("components/var/user.php");
 require_once("components/forms/textinput.php");
 if (isLoggedIn()) { header("Location: servers.php"); } //we don't want people that is already logged in...
$fail = "<br/>";
$success = false;
$inputs['userName'] = new TextInput("UserName","",
"Enter your user name.", "userName","textInput","",0,0,3,20,false,false);

if (!empty($_POST['submit'])) {
    $user = User::getUser($inputs['userName']->getValue());
    if ($user == null) {
        $fail = "Username is invaild";
    }
    else {
        $success = true;
    }
}

$template = new template();
$template->html_head("Lost Password");
$template->html_body_aboveContent();
if (!empty($_GET['key'])) { //stage 2
    $key = $mysql->escape($_GET['key']);
    $sql = "SELECT * FROM Users WHERE verifykey='$key'";
    $result = $mysql->query($sql);
    if ($result->num_rows) {
        $inputs['userPass'] = new TextInput("UserPass","",
        "Enter your new password.", "userPass","textInput","",0,0,7,200,true,false);
        $inputs['userConfirmPass'] = new TextInput("UserConfirmPass","",
        "Enter the same password.", "userConfirmPass","textInput","",0,0,7,200,true,false);
        if (!empty($_POST['submit'])) {
            if (!$inputs['userPass']->validate("")) {
                $fail = $inputs['userPass']->failMsg;
            }
            else if ($inputs['userPass']->getValue() != $inputs['userConfirmPass']->getValue()) {
                $fail = "Passwords does not match";
            }
            else { $success = true; }
        }
        if ($success) {
            $e = new encryptor($config);
            $pass = $e->SHA256Hash($inputs['userPass']->getValue());
            $sql = "UPDATE Users SET password='$pass' WHERE verifykey='$key'";
            $result = $mysql->query($sql);
            print "<h1>Success reset of your Password</h1>";
        }
        else {
        ?>
        <h1>Reset Password</h1>
    	<div class='bubble login'>
            <span style='color:red;'><?php print $fail; ?></span>
        	<form method='POST' action='lostpassword.php?key=<?php print $key; ?>'>
                <table>
                    <tr>
                	  <td>Password:</td>
                      <td><?php print $inputs['userPass']->createHtml(); ?></td>
                    </tr>
                    <tr>
                	  <td>Confirm Password:</td>
                      <td><?php print $inputs['userConfirmPass']->createHtml(); ?></td>
                    </tr>
                    <tr>
                        <td colspan='2'>
                            <input type='submit' name='submit' value='Submit' />
                        </td>
                    </tr>
                </table>
        	</form>
    	</div>
        <?php
        }
    }
    else {
        print "Key doesn't match";
    }
}
else {
    if (!$success) { //stage 1
    	?>
        <h1>Reset Password</h1>
    	<div class='bubble login'>
            <span style='color:red;'><?php print $fail; ?></span>
        	<form method='POST' action='lostpassword.php'>
                <table>
                    <tr>
                	  <td>Username:</td>
                      <td><?php print $inputs['userName']->createHtml(); ?></td>
                    </tr>
                    <tr>
                        <td colspan='2'>
                            <input type='submit' name='submit' value='Submit' />
                        </td>
                    </tr>
                </table>
        	</form>
    	</div>
    	<?php
    }
    else {
        print "<h1>Email sent!</h1>";
        email($user->getName(),$user->getEmail(),$user->getVerifyKey());
    }
}
 $template->html_body_belowContent();
 $template->html_body_footer();

function email($username,$email,$key) {
    global $config;
    require_once("components/util/smtp.php");
    // subject
    $subject = "Password Recovery";
    // message
    $url = "http://". $config->site_url ."/lostpassword.php?key=$key";
    $message = "<html>
        <head>
            <title>Verification to $config->site_name</title>
        </head>
        <body>
            $config->site_name's password recovery.<br/>
            Username: $username <br/>
            click the link to change your password.<br/>
            if the link doesn't work. paste it into your address bar.<br/>
            <a href='$url'>$url</a><br/>
            Have any problems, please email us at <a href='mailto:$config->site_email'>$config->site_email</a>.</br>
            <br/>
            <span style='font:8px;'>If you didn't have this sent to you,
            please reply this and explain that you did not register.</span>
        </body>
    </html>";

    $smtp = new SMTPClient($config->SmtpServer,$config->SmtpPort,$config->SmtpUser,$config->SmtpPass,
    $config->site_email,$email,$subject,$message);
    $smtp->SendMail();
}
?>