<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name help.php - the helpful page
 */
    if (!empty($_GET['target'])) {
        switch ($_GET['target']) {
            case "desc":
            ?>
            Server description describes your server and is the &quot;face&quot; of your server in the server profile.
            It supports BBcode, the following bbcodes currently works.<br />
            <div class='bubble' style='width:45em;text-align:left;'>
                <code>
                    [h1-6]text[/h1-6] - headers ex: [h1]MyCraft[/h1]<br />
                    [color=red]text[/color] - supports any color including hex.<br />
                    [colour=blue]text[/colour] - british version of [color]<br />
                    [code]text[/code] - for code formating.<br />
                    [align=center]text[/align] - for aligning stuff.<br />
                    [b]text[/b] - bold<br />
                    [i]text[/i] - italicize<br />
                    [u]text[/u] - underline<br />
                    [br] - break line, this will automaticly be added when you use enter in the textarea.<br />
                    [img width,height]url[/img] - display a image, width and height is required.<br />
                    [url=http://minecraftservers.com]Text to link[/url] - create a link.<br />
                    [yt width,height]code at the end of youtube videos[/yt] - display a video, width and height is required.
                    The code you use is the bolded (www.youtube.com/watch?v=<strong>PhrumGLBmYc</strong>).<br />
                </code>
                Use the bbcode as you would in forums and such.
            </div>
            <?php
            break;
            case "tags":
                //array("PvP","Hardcore PvP","PvE","Anarchy","Creative",
                //"Survival","RolePlay","White List","Vanilla","Bukkit","Custom");
                ?>
                Tags help users to filter out servers that interest them.
                <div class='bubble' style='width:45em;text-align:left;'>
                <ul>
                    <li>PvP : A server where players are encouraged to fight versus other players
                    (thus the appellation PvP, meaning Player versus Player). Stealing is sometimes allowed.</li>
                    <li>Hardcore PvP : A type of server which is like PvP, but fully allows griefing.</li>
                    <li>Prison : In prison servers, there typically is no wilderness,  and player must earn money in order to advance in the prison.
                    There are servers that allow you to gain freedom as you advance in the ranks of the prison, in which you can gain or earn your freedom.</li>
                    <li>PvE : Player versus Environment</li>
                    <li>Anarchy : Complete choas, very little rules other than against hacks.</li>
                    <li>Creative : Maybe creative on join or given when trust is earned.</li>
                    <li>Survival : Players must &quot;live off the land&quot;.</li>
                    <li>RolePlay : A server, usually with a normal map, where players are encouraged to take on roles like Mayor, Blacksmith, Cook, and act as them in the game.
                    These servers often attempt to simulate settlement in some foreign land or similar, and require the gathering of resources to build structures.
                    There are generally factions that can be made in these servers.</li>
                    <li>White List : Servers that require special applications to enter.</li>
                    <li>Vanilla : Servers with no or little modifications from notch's server software.</li>
                    <li>Bukkit : Servers with bukkit(or similar server wrapper) and with many plugins.</li>
                    <li>Custom : Servers with custom server software or vanilla/bukkit mods (not plugins) ex. Spout,(SpoutPlugin(if required)), etc</li>
                    <li>Offline Mode : Servers that have premium authecation turned off, all <strong>Hamachi servers</strong> should have these tag rather or not its turned off.</li>
                </ul>
                </div>
                <?php
            break;
            case "banner":
            ?>
                <div class='bubble' style='width:24em;'>
                The banner shall be autoresized to fit the area.<br />
                For best results, use the dimesions 468x60.
                </div>
            <?php
            break;
            case "votifier":
            ?>
            <div class='bubble' style='width:24em;'>
            Votifier is a bukkit plugin that notifies the server when someone votes for it.

            </div>
            <?php
            break;
        }

    }
 ?>