<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name logout.php
*/
 require_once("template/template.php");
 $logout = logout();
 $template = new template();

 $template->html_head("Logout");
 $template->html_body_aboveContent();

if ($logout) {
    print "<h1>You are now Logged out.</h1>";
    redirectHTMLtoReferer("");
}
 $template->html_body_belowContent();
 $template->html_body_footer();
 ?>