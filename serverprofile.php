<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name serverprofile.php
*/
 require_once("template/template.php");
 require_once("components/var/server.php");
 require_once("components/var/comment.php");
 require_once("components/forms/default/commentform.php");
 require_once("components/forms/default/simplepageform.php");
 require_once("components/util/logostatus.php");

$commentForm = new CommentForm();

$server = null;
if (!empty($_REQUEST['id'])) {
    $id = $mysql->escape($_REQUEST['id']);
    $server = Server::getServerByID($id);
    if ($server != null) {
        $title = $server->getName();
    }
}
if ($server == null) {
    header("Status: 404 Not Found");
    die();
}
    /** ajax for comments */
 if (!empty($_GET['code']) && $_GET['code'] === "Derpcomments") {
    showComments();
 }
 else {
    $template = new template();
    if ($server != null) {
        $template->html_head($server->getName());
        ?>
        <style type='text/css'>
        table td {
            text-align:left;
        }
        .rightTd {
            text-align: right;
            width:30%;
        }
        </style>
        <script type='text/javascript'>
            function loadPage(page,derp) {
                getUrlPage("comments","serverprofile.php?id=<?php print $server->getId(); ?>&code=Derpcomments&page="+page);
            }
        </script>
        <?php
    }
    else {
        $template->html_head("");
    }
    $commentForm->head();
    $template->html_body_aboveContent();
    ServerProfile($server);

    $template->html_body_belowContent();
    $template->html_body_footer();
    storeOldPage();
 }

 function ServerProfile($server) {
    global $config,$mysql,$loggedInUser,$commentForm;
    ?>
    <h1><?php print htmlentities($server->getName()); ?></h1>
    <div class='bubble'>
        <?php
        showDashBoard();
        ?>
        <div class='bubble ServerListItem <?php print ($server->isSponsered() ? "SponseredServerListItem" : "")?>'
         style='min-height:63px;width:473px;padding:5px;margin:0 auto 5px auto;'>
            <div class='ServerBanner' style='background-image: url("<?php print htmlentities($server->getLogo()); ?>");'>
                <?php
                    if ($server->isOnline()) {
                        print "<img src='template/Styles/Images/online.png' title='Online!' alt='Online!' />";
                    }
                    else {
                        print "<img src='template/Styles/Images/offline.png' title='Offline :(' alt='Offline!' />";
                    }
                ?>
            </div>

        </div>
        <div style='margin:auto;width=90%;'>
            <?php
            showGiveCookie();
            ?>
        </div>
        <div class='bubble ServerListItem <?php print ($server->isSponsered() ? "SponseredServerListItem" : "")?>'>
            <table style='margin:0 auto 5px auto;width:35%;'>
                <tr>
                    <td><strong>Owner:</strong></td>
                    <td>
                        <a href='userprofile.php?id=<?php print $server->getOwnerId(); ?>'>
                        <?php print htmlentities(User::getUserFromId($server->getOwnerId())->getName()); ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td><strong>Website:</strong></td>
                    <td>
                        <?php
                            if ($server->getWebsite()) {
                        ?>
                         <a href='<?php print htmlentities($server->getWebsite()); ?>' target='_BLANK'>
                             <?php print htmlentities($server->getWebsite()); ?>
                         </a>
                         <?php
                         }
                         else {
                            print "N/A";
                         }
                         ?>
                    </td>
                </tr>
                <?php
                $imgl = LogoStatus::getImage($server);
                if (file_exists($imgl)) {
                 ?>
                <tr>
                    <td colspan='2'>
                    <?php
                        $servloc = $config->site_url ."/serverprofile.php?id=".$server->getId();
                        $imgloc = $config->site_url ."/". $imgl;
                        $html = "&lt;a href=&quot;$servloc&quot;&gt;&lt;img src=&quot;$imgloc&quot; alt=&quot;McServerListings&quot;/&gt;&lt;/a&gt;";
                        $bbcode = "[url=$servloc][img]".$imgloc."[/img][/url]";
                    ?>
                        <a onclick='javascript:prompt("Copy this and enter it in your minecraft.","<?php print htmlentities($server->getIp() .":". $server->getPort()); ?>");' title='Click Here to copy ip.'>
                            <img src='<?php print $imgloc; ?>' alt='Minecraft Server Status Logo'/>
                        </a>
                    </td>
                </tr>
                <tr></tr>
                    <td>
                        HTML:
                    </td>
                    <td>
                        <input type='test' readonly='' value='<?php print $html; ?>'/>
                    </td>
                <tr>
                    <td>
                        BBcode:
                    </td>
                    <td>
                        <input type='test' readonly='' value='<?php print $bbcode ?>'/>
                    </td>
                </tr>
                <?php
                }
                else {
                ?>
                <tr>
                    <td><strong>Minecraft Server Ip:</strong></td>
                    <td>
                        <a onclick='javascript:prompt("Copy this and enter it in your minecraft.","<?php print htmlentities($server->getIp() .":". $server->getPort()); ?>");' title='Click Here to copy ip.'>
                        <?php print htmlentities($server->getIp()); ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td><strong>Port:</strong></td>
                    <td>
                         <?php print htmlentities($server->getPort()); ?>
                    </td>
                </tr>
                <tr>
                    <td><strong>Uptime:</strong></td>
                    <td>
                         <?php print htmlentities($server->getFormatedUptime()); ?>
                    </td>
                </tr>
                <tr>
                    <td><strong>Player Slots:</strong></td>
                    <td>
                         <?php print $server->getPlayerSlots(); ?>
                    </td>
                </tr>
                <?php
                }
                ?>
                <tr>
                    <td><strong>Continent:</strong></td>
                    <td>
                        <?php print $server->getContinent(); ?>
                    </td>
                </tr>
                <tr>
                    <td><strong>MOTD:</strong></td>
                    <td><?php print htmlentities($server->getMotd()); ?></td>
                </tr>
                <tr>
                    <td><strong>Tags:</strong></td>
                    <td>
                    <?php
                        $tags = $server->getTags();
                        $count = count($tags);
                        for ($i = 0; $i < $count; $i++) {
                        print "[". $tags[$i] ."] ";
                        }
                    ?>
                    </td>
                </tr>
            </table>
            <div class='bubble' style='background-color: #EFEED1;overflow:auto;max-height:500px;margin-bottom: 20px;'>
                <?php print $server->getSafeDescription(); ?>
            </div>
        </div>
        <div id='comments'>
            <?php
            if ($server->allowComments()) {
                if (isLoggedIn()) {
                    /**
                     * post new comment
                     */
                     if ($commentForm->Vaildate()) {
                        PostNewComment($server,$commentForm);
                     }
                     $commentForm->showForm(getExecutingPage(),true);
                     if (!empty($_GET['deletecomment'])) {
                        $cid = $mysql->escape($_GET['deletecomment']);
                        Comment::delete($cid);
                        print "<span style='color:green'>Deleted comment</span>";
                     }
                }
            }
            showComments();
            ?>
        </div>
    </div>
    <?php
 }

 function showComments() {
    global $mysql,$server,$loggedInUser,$commentForm;
    if ($server->allowComments()) {
        $limit = 10;
        $page = 0;
        if (!empty($_GET['page'])) {
            $page = $mysql->escape($_GET['page']) - 1;
        }
        $sql = "SELECT * FROM Comments WHERE serverId='". $server->getId()."'  ORDER BY time DESC LIMIT ". ($page * $limit).", $limit";
        $result = $mysql->query($sql);
        $comments = Comment::CommentsFromResult($result,null,$server);
        displayComments($comments,$page);

        $sql = str_replace("*","COUNT(*)",$sql);
        $r = $mysql->query($sql);
        $row = mysqli_fetch_array($r);
        $y = $row['COUNT(*)'];
        SimplePageForm::displayForm($y / $limit,$page,4,4);
    }
 }

 function displayComments($comments,$page) {
    global $loggedInUser,$server;
    $count = count($comments);
    for ($i = 0; $i < $count; $i++) {   
        $cUser = User::getUserFromId($comments[$i]->getOwnerId()); 
        ?>
            <table class='bubble comment'>
                <tbody>
                    <tr>
                        <td>
                            <a href='userprofile.php?id=<?php print $cUser->getId();?>'><?php print htmlentities($cUser->getName()); ?></a>
                        </td>                        
                        <td>
                            <span class='commentDate'>
                            <?php print date("F j, Y, g:i a",$comments[$i]->getTime()); ?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td width='75px'>
                            <?php
                            if (isLoggedIn()) {
                                if ($cUser->getId() == $loggedInUser->getId() || $loggedInUser->isAdmin()) {
                                    print "<div class='button' style='width:50px;'>\n
                                    <a href='serverprofile.php?id=". $server->getId() ."&page=$page&deletecomment=". $comments[$i]->getId() ."'>Delete</a>\n
                                    </div>\n";
                                }
                            }
                            else {
                                print "&nbsp";
                            }
                            ?>
                        </td>                        
                        <td>
                            <p class='commentMsg'><?php print htmlentities($comments[$i]->getMSG());?></p>
                        </td>
                    </tr>

                </tbody>
            </table>
        <?php
    }
    if (!$count) {
        print "Currently no comments..";
    }
 }

 function showDashBoard() {
    global $mysql, $loggedInUser, $server;
    if (isLoggedIn()) {
        if ($loggedInUser->getId() == $server->getOwnerId() || $loggedInUser->isAdmin()) {
           ?>
           <div style='margin-bottom:0.5em;'>
           <table style='margin:auto;'>
                <tr>
                    <td class='button'>
                        <a href='serveradmin.php?id=<?php print $server->getId(); ?>'>Manage Server</a>
                    </td>
                    <td class='button'>
                        <a href='sponsermanager.php?id=<?php print $server->getOwnerId(); ?>'>Manage Sponsership</a>
                    </td>
                </tr>
           </table>
           </div>
            <?php
        }
    }
    
}
 
 
/**
 * post new comment
 */
 function PostNewComment($server,$commentForm) {
    global $loggedInUser;
    Comment::create($loggedInUser,$server,$commentForm->inputs['commentMSG']->getValue());
 }

 function showGiveCookie() {
    global $mysql,$server,$loggedInUser;
    if (isset($_POST['givecookie'])) { 
        $fail = false;
        $ip = $_SERVER['REMOTE_ADDR'];
        $ign = "";
        if (!empty($_POST['IGN'])) {
            $ign = $mysql->escape($_POST['IGN']);
        }
        $time = 0;
        if (isLoggedIn()) {
            $time = $loggedInUser->vote($server,$ign,$ip);
        }
        else {
            $user = null;
            if (!$server->hasVotifier()) {
                $user = GuestUser::getUserFromIp($ip);
            }
            else {
                if (empty($ign)) {
                    $user = GuestUser::getUserFromIp($ip);
                }
                else {
                    $user = GuestUser::getUserFromIpOrIGN($ip,$ign);
                }
            }
            if ($user == null) {
                /** Make sure we stop spam, one vote per ip. */
                 $sql = "SELECT ip FROM Users WHERE ip='{$ip}'";
                 //if (!empty($ign)) $sql .= "OR ign='$ign'";
                 $result = $mysql->query($sql);
                 if ($result->num_rows) {
                   $row = $result->fetch_assoc();
                   print_r($row);
                    print "<div>You IP address matches an existing account.<br/>
                    If so please <a href='login.php'>Log in</a>.
                    </div>";
                    $fail = true;
                 }
                 else {
                    $user = GuestUser::createGuestUser($ign,$ip);
                    $time = $user->vote($server);
                }
            }
            else {
                $time = $user->vote($server);
            }
        }
        if (!$fail) {
            if ($time == 0) {
                print "<div><strong>Cookie given!</strong></div>";
            }
            else {
                print "<div><strong style='color:red;'>You already voted.</strong><br/>
                You can give another again after ".date("F j, Y, g:i a",$time) ."</div>";
            }
        }
    }
    print "<strong>Cookies:</strong>". $server->getCookies() ."
        <div>
            <form method='POST' action='serverprofile.php?id=". $server->getId() ."'>";
            if ($server->hasVotifier()) {
                print "<div>
                    Minecraft IGN:<input type='text' name='IGN' title='Minecraft Account Name' value='". (isLoggedIn() ? $loggedInUser->getIGN() : "") ."'/>
                </div>";
            }
    print "
        <input class='voteCookie' type='submit' name='givecookie' value='Vote'/>
    </form>
    </div>";
 }
 ?>