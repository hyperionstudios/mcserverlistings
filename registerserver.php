<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name registerserver.php
 */
 require_once("template/template.php");
 require_once("components/var/blacklist.php");
 require_once("components/var/server.php");
 require_once("components/forms/default/serverform.php");
 require_once("components/util/logostatus.php");
 require_once("components/var/transaction.php");
if (!isLoggedIn()) {
    storeOldPage();
    header("Location: login.php");
}
 $template = new template();
 $template->html_head("Submit a server");
 $serverform = $serverform = new ServerForm(null);
 $serverform->head();
 $template->html_body_aboveContent();
if (isLoggedIn()) {
    if ($loggedInUser->isVerified()) {
        print "<h1>Submit a Server</h1>";
        if ($serverform->Vaildate()) {
            Register($serverform);
        }
        $serverform->showForm(getExecutingPage(),false);
    }
    else {
        print "<h1>You are not a verified user</h1>
        You need to verify your email first.<br/>
        <a href='register.php?resend=1'>Resend Confirmation Email</a>";
    }
}
$template->html_body_belowContent();
$template->html_body_footer();

function Register($serverform) {
   global $config, $mysql,$loggedInUser,$purifier;
        $name = $serverform->inputs['serverName']->getValue();
        $ip = $serverform->inputs['serverIp']->getValue();
        $port = $serverform->inputs['serverPort']->getValue();
        $unsafeDescription = $serverform->inputs['serverDescription']->getValue();
        $logo = $serverform->inputs['serverLogo']->getValue();
        $continent = $serverform->inputs['serverContinent']->getValue();
        $allowComments = $serverform->inputs['AllowComments']->isChecked();
        $website = $serverform->inputs['serverWebsite']->getValue();
        $votifier = $serverform->inputs['Votifier']->isChecked();
        $votifierIp= $serverform->inputs['VotifierIp']->getValue();
        $votifierPort= $serverform->inputs['VotifierPort']->getValue();
        $votifierRSAKey= $serverform->inputs['VotifierRSAKey']->getValue();

        $success = true;

        $sql = "SELECT * FROM Servers WHERE (name='$name' OR (ip='$ip' AND port='$port')) OR owner='". $loggedInUser->getId() ."'";
        $result = $mysql->query($sql);
        $c = 0;
        while ($rows = $result->fetch_array()) {
            if (strtolower($name) == strtolower($rows['name'])) {
                $serverform->fail = "Server already exists with that name.";
                $success = false;
                break;
            }
            else if (strtolower($ip) == strtolower($rows['ip'])) {
                if ($port == $rows['port']) {
                    $serverform->fail = "A server already exists with that ip and port.";
                    $success = false;
                    break;
                }
            }
            else if ($rows['owner'] == $loggedInUser->getId()) {
                if (++$c > $config->maxServersPerUser) {
                    $serverform->fail = "Sorry, but you have reached your limit of server listings.";
                    $success = false;
                }
            }
        }
        if ($success){
            $safeDescription = $purifier->purify($unsafeDescription);
            success($serverform,$name,$ip,$port,$website,$continent,$unsafeDescription,$safeDescription,$allowComments,$logo,$votifier,$votifierIp,$votifierPort,$votifierRSAKey);
        }
}

function success($serverform,$name,$ip,$port,$website,$continent,$unsafeDescription,$safeDescription,$allowComments,$logo,$votifier,$votifierIp,$votifierPort,$votifierRSAKey) {
    global $mysql,$loggedInUser;
    $tags = array();
    $count = count($serverform->tags);
    for ($i=0; $i<$count; $i++) {
        if ($serverform->tags[$i]->isChecked()) {
            array_push($tags,$serverform->tags[$i]->getValue());
        }
    }
    if ($server = Server::create($name,$loggedInUser,$ip,$port,$website,$continent,$unsafeDescription,$safeDescription,$allowComments,$logo,$tags,$votifier,$votifierIp,$votifierPort,$votifierRSAKey)) {
        print "<div>
            <h2>Listed $name.</h2>
        </div>";
        redirectHTMLtoReferer("serverprofile.php?id=". $server->getId());
        LogoStatus::create($server);
        /* Promo: give three tokens to the 500th. 
        if (Server::getNextServerId() == 501 || $loggedInUser->getId() == 1) {
            $loggedInUser->giveToken(3);
            print "<div>Congrats! You just submitted the 500th server!<br/>Three tokens has be credited to your account.</div>\n";
            Transaction::createTransaction("Promo", time(),"0.00",$loggedInUser->getId()); 
        }*/
    }
    else {
        print "Internal error.";
    }
}
?>


