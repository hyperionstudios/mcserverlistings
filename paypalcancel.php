<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name premium.php
*/
 require_once("template/template.php");
 require_once("components/var/user.php");
 require_once("components/var/server.php");
 require_once("components/util/serverutil.php");
 $template = new template();

 $template->html_head("");
 $template->html_body_aboveContent();
 ?>
 <h1>Sorry you canceled. :(</h1>
 <script>
alert("Payment Cancelled"); 
 // add relevant message above or remove the line if not required
window.onload = function(){
	 if(window.opener){
		 window.close();
	} 
	 else{
		 if(top.dg.isOpen() == true){
              top.dg.closeFlow();
              return true;
          }
     }                              
};                             
</script>
<?php
 $template->html_body_belowContent();
 $template->html_body_footer();
?>