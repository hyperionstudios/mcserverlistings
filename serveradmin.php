<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name serveradmin.php
*/
 require_once("template/template.php");
 require_once("components/var/server.php");
 require_once("components/forms/admin/serverform.php");
 $template = new template();

 $template->html_head("Server Admin");
    if (!empty($_GET['id'])) {
         $id = $mysql->escape($_GET['id']);
         $server = Server::getServerByID($id);
         if ($server != null) {
            $serverform = new AdminServerForm($server);
            $serverform->head();
         }
    }
 $template->html_body_aboveContent();
 if (isLoggedIn()) {
     if ($server != null) {
        if ($loggedInUser->getId() == $server->getOwnerId() || $loggedInUser->isAdmin()) {
             print "<h1>
                        Managing
                        <a href='serverprofile.php?id=".$server->getId()."'>
                            ".htmlentities($server->getName())."
                        </a>
                    </h1>";
             $result = $serverform->Vaildate();
             if ($result !== "delete" ) {
                if ($result) {
                  $serverform->UpdateServer($serverform);                 
                  if ($serverform->inputs['serverSponser']->isChecked()) {
                    $server->giveSponsership();
                  }
                }
                $serverform->showForm(getExecutingPage(),true);
             }
             else {
                Server::delete($server);
             }
         }
         else {
            print "<h1>Permisson Denied</h1>";
            redirectHTMLtoReferer("",1);
         }
     }
     else {
        print "<h1>No server exists with that id.</h1>";
        redirectHTMLtoReferer("");
     }
 }
 else {
    storeOldPage();
    redirectHTMLtoReferer("login.php",0);
 }
 $template->html_body_belowContent();
 $template->html_body_footer();

 ?>