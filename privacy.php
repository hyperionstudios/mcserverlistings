<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name privacy.php
 */
 require_once("template/template.php");
 $template = new template();

 $template->html_head("Privacy Policy");
 $template->html_body_aboveContent();
 ?>
 <h2>Privacy Policy</h2>
 <div class='bubble'>
    <p>
      <?php print $config->site_name; ?> respects each individual's right to personal privacy.
      We will collect and use information through our Web site only in the ways disclosed in this statement.
      This statement applies solely to information collected at the <?php print $config->site_name; ?> Web site.
    </p>
    <p>
      <h3>Part I. Information Collection</h3>
      <?php print $config->site_name; ?> collects information through our Web site at several points.
    </p>
    <p>
      <?php print $config->site_name; ?> does not actively market to children,
      and we never knowingly ask a child under 13 to divulge personal information.
    </p>
    <p>
      We collect the following general data that is not personally identifiable information: IP address, hostname.
      We collect demographic data in order to determine our reach in different countries and regions.
    </p>
    <p>
      We collect this information through automated logging via our webserver
      and by ad tracking done through 3rd party related offerings.
      Also user accounts on our website and our forums.
      The information is collected voluntarily, the automated logs are obviously just a product of standard network browsing.
    </p>
    <p>
      We do employ cookies. A cookie is a small text file that our Web server places on a user's computer hard drive to be a unique identifier.
      Cookies enable <?php print $config->site_name; ?> to track usage patterns and deliver customized content to users. Our cookies do have an expiration date.
      Our cookies do collect personally identifiable information. The reason for this is to allow the user to logon to areas of our site automatically.
      This information is simply tied into your account here at <?php print $config->site_name; ?>.
    </p>
    <p>
      Banner advertising appearing on our Web site may collect the following information: hostname/ip address, geo targeting.
    </p>
    <p>
      <h3>Part II. Information Usage.</h3>
    </p>
    <p>
      The information collected by <?php print $config->site_name; ?> will be used for primary access to our site,
      as well as statistical information for our own usage.
    </p>
    <p>
      Registered users will receive additional announcements from us about products, services, special deals, and/or a newsletter.
      Out of respect for the privacy of our users we present the option to not receive these types of communications based on individual feedback from the user.
    </p>
    <p>
      The information we collect will not be used to create customer profiles based on browsing or purchasing history. We will not supplement information collected at our Web site with data from other sources.
    </p>
    <p>
      We will not share data with any 3rd parties.
    </p>
    <p>
      We offer links to other Web sites.
      Please note: When you click on links to other Web sites, we encourage you to read their privacy policies.
      Their standards may differ from ours.
    </p>
    <p>
        If our policy on information collection or uses changes, we will update this privacy policy.
    </p>
    <p>
        <h3>Part III. Access to Information</h3>
    </p>
    <p>
        <?php print $config->site_name; ?> maintains the accuracy of our information by allowing the user to maintain and update his/her own information.
        Users may access their own personal information and contact us about inaccuracies they may find.
        Users may delete their information from our database.
    </p>
    <p>
      <h3>Part IV. Problem Resolution</h3>
    </p>
    <p>
      If problems arise, users may contact us by email to
      <a href='mailto:<?php print $config->site_email; ?>'><?php print $config->site_email; ?></a>.
      We are committed to resolving disputes within 7 days.
    </p>
    <p>
      <h3>Part V. Data Storage and Security</h3>
    <p>
    <p>
      <?php print $config->site_name; ?> protects user information with the following security measures:
      <strong>firewalls, restricted access, password authentication, and SHA256 salted hashed passwords.</strong>
    </p>
 </div>

 <?php
 $template->html_body_belowContent();
 $template->html_body_footer();
  storeOldPage();
 ?>