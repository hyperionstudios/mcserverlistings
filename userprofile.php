<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name userprofile.php
 */
 require_once("template/template.php");
 require_once("components/var/server.php");
 require_once("components/util/serverutil.php");
 $user = null;
 if (!empty($_GET['id'])) {
    if (isLoggedIn()) {
        if ($loggedInUser->getId() == $_GET['id']) {
            $user = $loggedInUser;
        }
        else {
            $user = User::getUserFromId($_GET['id']);
        }
    }
    else {
        $user = User::getUserFromId($_GET['id']);
    }
 }
 else {
    if (isLoggedIn())
        $user = $loggedInUser;
 }
 $template = new template();
 $template->html_head("User Profile - ". ($user == null ? "" : htmlentities($user->getName())));
 $template->html_body_aboveContent();

if ($user != null) {
    showDashBoard();
    storeOldPage();
}
else {
    print "<h1>User does not exist</h5>";
    redirectHTMLtoReferer("");
}
$template->html_body_belowContent();
$template->html_body_footer();

function showDashBoard() {
    global $mysql, $loggedInUser,$user;
    if (isLoggedIn()) {
        if ($user->getId() == $loggedInUser->getId() || $loggedInUser->isAdmin()) {
           ?>
           <div class='bubble'>
           <table style='margin:auto;'>
                <tr>
                    <td class='button'>
                        <a href='useradmin.php?id=<?php print $user->getId(); ?>'>Edit Profile</a>
                    </td>
                    <td class='button'>
                        <a href='sponsermanager.php?id=<?php print $user->getId(); ?>'>Manage SponserShip</a>
                    </td>
                </tr>
           </table>
            <?php
        }
    }
    showServers();
    print "</div>\n";
}
function showServers() {
    global $mysql, $loggedInUser,$user;
    $sql = "SELECT * FROM Servers WHERE owner='". $user->getId() ."'";
    //print $sql;
    $result = $mysql->query($sql);
    $servers = Server::serversFromResult($result);
    $count = count($servers);
    print "<h1>". ((isLoggedIn() && $user->getId() == $loggedInUser->getId()) ? "My" : $user->getName()."'s") ." Servers</h1>\n";
    if ($count == 0) { 
        print "<h5>". (($user->getId() == $loggedInUser->getId()) ? "You" : $user->getName())." does not have any servers listed.</h3>";
    }
    for ($i=0; $i < $count; $i++) {
        serverUtil::simpleDisplayServer($servers[$i],true);
    }
}

?>


