<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name premium.php
*/
 require_once("template/template.php");
 require_once("components/var/user.php");
 require_once("components/var/server.php");
 require_once("components/util/serverutil.php");
 $template = new template();

 $template->html_head("Premium");
 $template->html_body_aboveContent();
 ?>
 <h1>Subscribe for a month worth of sponsership!</h1>
 <div class='bubble'>
<h3>Benefits:</h3>
<ul style='width:50%;margin:auto;text-align:left;'>
    <li>Premium Listing spot atop the homepage.</li>
    <li>Golden Highlighted listing in the regular directory and server profile page.</li>
    <li>Top priority for support.</li>
</ul>
 <?php
 if (Server::getSponseredServerCount(true) >= $config->maxSponsers) {
    print "<p style='color:red;'>Warning: We have reached our max sponsers this month, so you will not be able to use your token at this moment.</p>";
 }
 //display our button.
?>
<script src='https://www.paypalobjects.com/js/external/dg.js' type="text/javascript"></script>
<script type='text/javascript'>
    var dg = new PAYPAL.apps.DGFlow(
    {
        trigger: 'paypal_submit',
        expType: 'instant'
         //PayPal will decide the experience type for the buyer based on his/her 'Remember me on your computer' option.
    });

</script>

    <br/>
    <div>Purchase &quot;Sponser&quot; Tokens, with these you can give your server sponsership status!</div>
    <form action='paypalcheckout.php' method='POST'>
        Quantity:<input type='text' name='quantity' style='width:20px;' value='1'/><br/>
        <input type='image' name='paypal_submit' id='paypal_submit'  src='https://www.paypal.com/en_US/i/btn/btn_dg_pay_w_paypal.gif' alt='Pay with PayPal'/>
    </form>
<?php
 print "<h5>Current sponsers.</h5>";
 $servers = Server::getSponseredServers();
 $count = count($servers);
 for ($i = 0; $i < $count; $i++) {
    serverUtil::simpleDisplayServer($servers[$i],true);
 }
 print "</div>";
 $template->html_body_belowContent();
 $template->html_body_footer();

?>