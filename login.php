<?php
/**
 * McServerListing
 * @version: 1.0
 * @author: Pickle
 * @copyright 2012
 * @name login.php
*/
 require_once("template/template.php");
 require_once("components/forms/textinput.php");
 require_once("components/var/blacklist.php");

 $success = false;

 $fail = "<br/>";
 $inputs['userName'] = new TextInput("UserName","",
  "Enter your username", "userName","textInput","",0,0,3,20,false,false);
 $inputs['userPass'] = new TextInput("UserPass","",
  "Enter your password.", "userPass","textInput","",0,0,7,200,true,false);

 if (!empty($_POST['submit'])) {
    $success = login($inputs['userName']->getValue(),$inputs['userPass']->getValue());
 }
 $template = new template();

 $template->html_head("Login");
 $template->html_body_aboveContent();
 if ($blacklist = BlacklistUser::getBlacklistFromIp($ip)) {
    print "<div style='color:red'>
    <h1>You have been banned from this site.</h1>
    Reason: ". $blacklist->getReason() ."
    </div>";
}
else if (!$success) {
	?>
    <h1>Login</h1>
	<div class='bubble login'>
        <span style='color:red;'><?php print $fail; ?></span>
    	<form method='POST' action='login.php'>
            <table>
                <tbody>
                    <tr>
                	  <td>Username:</td>
                      <td><?php print $inputs['userName']->createHtml(); ?></td>
                    </tr>
                	<tr>
                        <td>Password:</td>
                        <td><?php print $inputs['userPass']->createHtml(); ?></td>
                    </tr>
                    <tr>
                    <td>
                        Remember me
                        <input type='checkbox' name='remember' value='1' />
                    </td>
                    <td>
                        <input type='submit' name='submit' value='Login' />
                    </td>
                    </tr>
                    <tr>
                        <td>
                            <a href='lostpassword.php'>Lost password?</a>
                        </td>
                        <td>
                            Not a member? <a href='register.php'>Register now!</a>
                        </td>
                    </tr>
                </tbody>
            </table>
    	</form>
        <p style='font-size: 10px;color: #CE4910'>
        Notice: this site relies on cookies and javascript enabled,
        if you have them turned off, please enable them. Thank you.
        </p>
	</div>
	<?php
}
else {
    print "<h1>You are now logged in.</h1>";
    redirectHTMLtoReferer("");
}
 $template->html_body_belowContent();
 $template->html_body_footer();

function login($username,$pass) {
    global $mysql,$config,$fail;
    /** This is incase we forget our password....lul
      $e = new encryptor($config);
      print $e->SHA256Hash($pass);
     */
    $user = User::getUser($username);
	if ($user != null) {
	    if ($user->login($pass)) {
          $_SESSION['user'] = $user;
          if (!empty($_POST['remember'])) {
            $expire = time()+60*60*24;
            setCookie("user",$user->getName(),$expire);
            setCookie("password",$pass,$expire);
          }
 	      return true;
        }
        else {
            $fail = "Incorrect password.";
        }
	}
	else {
	 $fail = "Username does not exist.";
	}
    return false;
 }
?>